# ISO 26262 Part 6 Requirements

This document outlines the requirements from ISO 26262 Part 6, focusing on the functional safety of electrical/electronic systems in production automobiles.

# Table of Contents

- [Introduction](#introduction)
- [Scope](#scope)
- [Normative References](#normative-references)
- [Terms and Definitions](#terms-and-definitions)
- [Requirements for Compliance](#requirements-for-compliance)
- [Functional Safety Assessment Process](#functional-safety-assessment-process)

# Introduction


# 1. Scope


# 2. Normative References


# 3. Terms and Definitions



# 4. Requirements for Compliance

# 4.1 Purpose

This clause describes how:
- To achieve compliance with the ISO 26262 series of standards.
- To interpret the tables used in the ISO 26262 series of standards.
- To interpret the applicability of each clause, depending on the relevant ASIL(s).

# 4.2 General Requirements

When claiming compliance with the ISO 26262 series of standards, each requirement shall be met, unless one of the following applies:
- Tailoring of the safety activities in accordance with ISO 26262-2 has been performed that shows that the requirement does not apply.
- A rationale is available that the non-compliance is acceptable and the rationale has been evaluated in accordance with ISO 26262-2.

Informative content, including notes and examples, is only for guidance in understanding, or for clarification of the associated requirement, and shall not be interpreted as a requirement itself or as complete or exhaustive. The results of safety activities are given as work products. “Prerequisites” are information which shall be available as work products of a previous phase. Given that certain requirements of a clause are ASIL-dependent or may be tailored, certain work products may not be needed as prerequisites. “Further supporting information” is information that can be considered, but which in some cases is not required by the ISO 26262 series of standards as a work product of a previous phase and which may be made available by external sources that are different from the persons or organizations responsible for the functional safety activities.

# 4.3 Interpretations of Tables

Tables are normative or informative depending on their context. The different methods listed in a table contribute to the level of confidence in achieving compliance with the corresponding requirement. Each method in a table is either:
- A consecutive entry (marked by a sequence number in the leftmost column, e.g. 1, 2, 3).
- An alternative entry (marked by a number followed by a letter in the leftmost column, e.g. 2a, 2b, 2c).

For consecutive entries, all listed highly recommended and recommended methods in accordance with the ASIL apply. It is allowed to substitute a highly recommended or recommended method by others not listed in the table, in this case, a rationale shall be given describing why these comply with the corresponding requirement. If a rationale can be given to comply with the corresponding requirement without choosing all entries, a further rationale for omitted methods is not necessary. For alternative entries, an appropriate combination of methods shall be applied in accordance with the ASIL indicated, independent of whether they are listed in the table or not. If methods are listed with different degrees of recommendation for an ASIL, the methods with the higher recommendation should be preferred. A rationale shall be given that the selected combination of methods or even a selected single method complies with the corresponding requirement. NOTE A rationale based on the methods listed in the table is sufficient. However, this does not imply a bias for or against methods not listed in the table. For each method, the degree of recommendation to use the corresponding method depends on the ASIL and is categorized as follows:
- “++” indicates that the method is highly recommended for the identified ASIL.
- “+” indicates that the method is recommended for the identified ASIL.
- “o” indicates that the method has no recommendation for or against its usage for the identified ASIL.

# 4.4 ASIL-dependent Requirements and Recommendations

The requirements or recommendations of each sub-clause shall be met for ASIL A, B, C and D, if not stated otherwise. These requirements and recommendations refer to the ASIL of the safety goal. If ASIL decomposition has been performed at an earlier stage of development, in accordance with ISO 26262-9:2018, Clause 5, the ASIL resulting from the decomposition shall be met. If an ASIL is given in parentheses in the ISO 26262 series of standards, the corresponding sub-clause shall be considered as a recommendation rather than a requirement for this ASIL. This has no link with the parenthesis notation related to ASIL decomposition.

# 4.5 Adaptation for Motorcycles

For items or elements of motorcycles for which requirements of ISO 26262-12 are applicable, the requirements of ISO 26262-12 supersede the corresponding requirements in this document. Requirements of ISO 26262‑2 that are superseded by ISO 26262-12 are defined in Part 12.

# 4.6 Adaptation for Trucks, Buses, Trailers and Semi-trailers

Content that is intended to be unique for trucks, buses, trailers and semi-trailers is indicated as such.


# 5. General topics for the product development at the software level

# 5.1 Objectives

The objectives of this clause are:
- To ensure a suitable and consistent software development process.
- To ensure a suitable software development environment.

# 5.3 Inputs to this Clause

## 5.3.1 Prerequisites

The following information shall be available:
- (none)

## 5.3.2 Further Supporting Information

The following information can be considered:
- Qualified software tools available (see ISO 26262-8:2018, Clause 11).
- Design and coding guidelines for modelling, design, and programming languages (from an external source).
- Guidelines for the application of methods (from an external source).
- Guidelines for the application of tools (from an external source).

# 5.4 Requirements and Recommendations


## 5.4.1 Software Development Processes and Environments

When developing the software of an item, software development processes and software development environments shall be used which:
- Are suitable for developing safety-related embedded software, including methods, guidelines, languages, and tools.
- Support consistency across the sub-phases of the software development lifecycle and the respective work products.
- Are compatible with the system and hardware development phases regarding required interaction and consistency of exchange of information.

**NOTE 1**: The sequencing of phases, tasks, and activities, including iteration steps, for the software of an item intends to ensure the consistency of the corresponding work products with the product development at the hardware level (see ISO 26262-5) and the product development at the system level (see ISO 26262-4).

**NOTE 2**: The software tool criteria evaluation report (see ISO 26262-8:2018, 11.5.1) or the software tool qualification report (see ISO 26262-8:2018, 11.5.2) can provide input to the tool usage.



## 5.4.2 Criteria for Selecting Design, Modelling, or Programming Languages

The criteria that shall be considered when selecting a design, modelling, or programming language are:
- An unambiguous and comprehensible definition.
- Suitability for specifying and managing safety requirements according to ISO 26262-8:2018 Clause 6, if modelling is used for requirements engineering and management.
- Support the achievement of modularity, abstraction, and encapsulation.
- Support the use of structured constructs.

**NOTE**: Assembly languages can be used for those parts of the software where the use of high-level programming languages is not appropriate, such as low-level software with interfaces to the hardware, interrupt handlers, or time-critical algorithms. However, using assembly languages can require a suitable application or tailoring of all software development phases (e.g., requirements of Clause 8).

## 5.4.3 Criteria for Suitable Modelling, Design, or Programming Languages

Criteria for suitable modelling, design, or programming languages (see 5.4.2) that are not sufficiently addressed by the language itself shall be covered by the corresponding guidelines, or by the development environment, considering the topics listed in Table 1.

**EXAMPLE 1**: MISRA C (see Reference [3]) is a coding guideline for the programming language C and includes guidance for automatically generated code.

**EXAMPLE 2**: In the case of model-based development with automatic code generation, guidelines can be applied at the model level as well as the code level. Appropriate modelling style guides including the MISRA AC series can be considered. Style guides for commercial tools are also possible guidelines.

**NOTE**: Existing coding guidelines and modelling guidelines can be modified for a specific item development.

## Table 1 — Topics to be Covered by Modelling and Coding Guidelines

| Topics                                             | ASIL A | ASIL B | ASIL C | ASIL D |
|---------                                           |--------|--------|--------|--------|
| 1a Enforcement of low complexity                   |   ++   |   ++   |     ++ |     ++ |
| 1b Use of language subsets                         |   ++   |   ++   |     ++ |     ++ |
| 1c Enforcement of strong typing                    |   ++   |   ++   |     ++ |     ++ |
| 1d Use of defensive implementation techniques      |   +    |   +    |     ++ |     ++ |
| 1e Use of well-trusted design principles           |   +    |   +    |     ++ |     ++ |
| 1f Use of unambiguous graphical representation     |   +    |   ++   |     ++ |     ++ |
| 1g Use of style guides                             |   +    |   ++   |     ++ |     ++ |
| 1h Use of naming conventions                       |   ++   |   ++   |     ++ |     ++ |
| 1i Concurrency aspects                             |   +    |   +    |      + |      + |

**NOTE**: An appropriate compromise of this topic with other requirements of this document may be required.

## 5.5 Work Products

<treqs-element id="ed9ef676061411ef8c6c0bd417f19d39" type="boundaryObject">
### 5.5.1 Documentation of the Software Development Environment
Resulting from requirements 5.4.1 to 5.4.3 and C.4.1 to C.4.11.
</treqs-element>
<treqs-element id="4d971572481411ef9fb373c5ff3bd930" type="role">
Development Team Lead
<treqs-link type="responsibleFor" target="ed9ef676061411ef8c6c0bd417f19d39" />

</treqs-element>



# 6. Specification of Software Safety Requirements

## 6.1 Objectives

The objectives of this sub-phase are:
- To specify or refine the software safety requirements which are derived from the technical safety concept and the system architectural design specification.
- To refine the requirements of the hardware-software interface initiated in ISO 26262-4:2018, Clause 6.
- To define the safety-related functionalities and properties of the software required for the implementation.
- To verify that the software safety requirements and the hardware-software interface requirements are suitable for software development and are consistent with the technical safety concept and the system architectural design specification.

## 6.2 General

The technical safety requirements are refined and allocated to hardware and software during the system architectural design phase given in ISO 26262-4:2018, Clause 6. The specification of the software safety requirements considers in particular constraints of the hardware and the impact of these constraints on the software. This sub-phase includes the specification of software safety requirements to support the subsequent design phases.

## 6.3 Inputs to this Clause

## 6.3.1 Prerequisites

The following information shall be available:
- Technical safety requirements specification in accordance with ISO 26262-4:2018, 6.5.1.
- Technical safety concept in accordance with ISO 26262-4:2018, 6.5.2.
- System architectural design specification in accordance with ISO 26262-4:2018, 6.5.3.
- Hardware-software interface (HSI) specification in accordance with ISO 26262-4:2018, 6.5.4.
- Documentation of the software development environment in accordance with 5.5.1.

## 6.3.2 Further Supporting Information

The following information can be considered:
- Hardware design specification (see ISO 26262-5:2018, 7.5.1).
- Specification of non-safety-related functions and properties of the software (from an external source).

## 6.4 Requirements and Recommendations

### 6.4.1 The Software Safety Requirements

The software safety requirements shall be derived considering the required safety-related functionalities and properties of the software, whose failures could lead to the violation of a technical safety requirement allocated to software.

**NOTE 1**: The software safety requirements are either derived directly from the technical safety requirements allocated to software or are requirements for software functions and properties that, if not fulfilled, could lead to a violation of the technical safety requirements allocated to software.

**EXAMPLE 1**: Safety-related functionality of the software can be functions that enable the safe execution of a nominal function, functions that enable the system to achieve or maintain a safe state or degraded state, functions related to the detection, indication, and mitigation of faults of safety-related hardware elements, self-test or monitoring functions related to the detection, indication, and mitigation of failures in the operating system, basic software, or the application software itself, functions related to on-board and off-board tests during production, operation, service, and decommissioning, functions that allow modifications of the software during production and service, or functions related to performance or time-critical operations.

**EXAMPLE 2**: Safety-related properties include robustness against erroneous inputs, independence or freedom from interference between different functionalities, or fault tolerance capabilities of the software.

**NOTE 2**: Safety-oriented analyses (see 7.4.10 or 7.4.11) can be used to identify additional software safety requirements or provide evidence for their achievement.

### 6.4.2 Specification of the Software Safety Requirements

Specification of the software safety requirements derived from the technical safety requirements, the technical safety concept, and the system architectural design in accordance with ISO 26262-4:2018, 6.4.1, and 6.4.3 shall consider:
- The specification and management of safety requirements in accordance with ISO 26262-8:2018, Clause 6.
- The specified system and hardware configurations. Configuration parameters can include gain control, band pass frequency, and clock prescaler.
- The hardware-software interface specification.
- The relevant requirements of the hardware design specification.
- The timing constraints. **EXAMPLE 2**: Execution or reaction time derived from the required response time at the system level.
- Communication and user interfaces. **EXAMPLE 3**: Communication and user interfaces.
- The external interfaces.
- Each operating mode and each transition between the operating modes of the vehicle, the system, or the hardware, having an impact on the software. **EXAMPLE 4**: Operating modes include shut-off or sleep, initialization, normal operation, degraded, and advanced modes for testing or flash programming.

### 6.4.3 If ASIL Decomposition is Applied

If ASIL decomposition is applied to the software safety requirements, ISO 26262-9:2018, Clause 5, shall be complied with.

### 6.4.4 The Hardware-Software Interface Specification

The hardware-software interface specification initiated in ISO 26262-4:2018, Clause 6, shall be refined sufficiently to allow for the correct control and usage of the hardware by the software, and shall describe each safety‑related dependency between hardware and software.

### 6.4.5 If Other Functions are Carried Out

If other functions in addition to those functions for which safety requirements are specified in 6.4.1 are carried out by the embedded software, a specification of these functions and their properties in accordance with the applied quality management system shall be available.

### 6.4.6 The Refined Hardware-Software Interface Specification

The refined hardware-software interface specification shall be verified jointly by the persons responsible for the system, hardware, and software development.

### 6.4.7 The Software Safety Requirements and the Refined Requirements

The software safety requirements and the refined requirements of the hardware-software interface specification shall be verified in accordance with ISO 26262-8:2018, Clauses 6 and 9, to provide evidence for their suitability for software development, compliance with the system design, compliance and consistency with the technical safety requirements, and consistency with the hardware-software interface.

## 6.5 Work Products

<treqs-element id="a3772892069c11efaea1a155b1602379" type="boundaryObject">
### 6.5.1 Software Safety Requirements Specification
Software safety requirements specification resulting from requirements 6.4.1 to 6.4.3 and 6.4.5.
</treqs-element>

<treqs-element id="ba3bc6d4491111ef9fb373c5ff3bd930" type="role">
 Mr. John Doe- Senior QA Engineer
<treqs-link type="responsibleFor" target="a3772892069c11efaea1a155b1602379" />
</treqs-element>

<treqs-element id="28ba3cb406e411efaea1a155b1602379" type="boundaryObject">
### 6.5.2 Hardware-Software Interface (HSI) Specification (Refined)
Hardware-software interface (HSI) specification (refined) resulting from requirement 6.4.4.
**NOTE**: This work product refers to the same work product as given in ISO 26262-5:2018, 7.5.1.
</treqs-element>


### 6.5.3 Software verification report 
resulting from requirements 6.4.6 and 6.4.7.


# 7. Software Architectural Design

## 7.1 Objectives

The objectives of this sub-phase are:
- To develop a software architectural design that satisfies the software safety requirements and the other software requirements.
- To verify that the software architectural design is suitable to satisfy the software safety requirements with the required ASIL.
- To support the implementation and verification of the software.

## 7.2 General

The software architectural design represents the software architectural elements and their interactions in a hierarchical structure. Static aspects, such as interfaces between the software components, as well as dynamic aspects, such as process sequences and timing behavior, are described.

**NOTE**: The software architectural design is not necessarily limited to one microcontroller or ECU. The software architecture for each microcontroller is also addressed by this sub-clause. A software architectural design is able to satisfy both the software safety requirements as well as the other software requirements. Hence, in this sub-phase, safety-related and non-safety-related software requirements are handled within one development process. The software architectural design provides the means to implement both the software requirements and the software safety requirements with the required ASIL and to manage the complexity of the detailed design and the implementation of the software.

## 7.3 Inputs to this Clause

### 7.3.1 Prerequisites

The following information shall be available:
- Documentation of the software development environment in accordance with 5.5.1.
- Hardware-software interface (HSI) specification (refined) in accordance with 6.5.2.
- Software safety requirements specification in accordance with 6.5.1.

### 7.3.2 Further Supporting Information

The following information can be considered:
- Technical safety concept (see ISO 26262-4:2018, 6.5.2).
- System architectural design specification (see ISO 26262-4:2018, 6.5.3).
- Qualified software components available (see ISO 26262-8:2018, Clause 12).
- Specification of non-safety-related functions and properties of the software and other software requirements in accordance with 6.4.5 (from an external source).

## 7.4 Requirements and Recommendations

### 7.4.1 To Avoid Systematic Faults

To avoid systematic faults in the software architectural design and in the subsequent development activities, the description of the software architectural design shall address the following characteristics supported by notations for software architectural design as listed in Table 2:
- Comprehensibility.
- Consistency.
- Simplicity.
- Modularity.
- Verifiability.
- Abstraction.
- Encapsulation.
- Maintainability.

**NOTE**: Abstraction can be supported by using hierarchical structures, grouping schemes or views to cover static, dynamic or deployment aspects of an architectural design.

### 7.4.2 During the Development

During the development of the software architectural design, the following shall be considered:
- The verifiability of the software architectural design.
- The suitability for configurable software.
- The feasibility for the design and implementation of the software units.
- The testability of the software architecture during software integration testing.
- The maintainability of the software architectural design.

### 7.4.3 In Order to Avoid Systematic Faults

In order to avoid systematic faults, the software architectural design shall exhibit the following characteristics by use of the principles listed in Table 3:
- Appropriate hierarchical structure of the software components.
- Strong cohesion within each software component.
- Restricted size and complexity of software components.
- Loose coupling between software components.
- Restricted size of interfaces.
- Appropriate scheduling properties.
- Appropriate management of shared resources.
- Restricted use of interrupts.
- Appropriate spatial isolation of the software components.

**NOTE 1**: An appropriate compromise between the principles listed in Table 3 can be necessary since the principles are not mutually exclusive.

**NOTE 2**: Indicators for high complexity can be highly branched control or data flow, excessive number of requirements allocated to single design elements, excessive number of interfaces of one design element or interactions between design elements, complex types or excessive number of parameters, excessive number of global variables, difficulty in providing evidence for suitability and completeness of error detection and handling, difficulty in achieving the required test coverage, or comprehensibility only to a few experts or only to project participants.

**NOTE 3**: These properties and principles also apply to software routines (e.g. service routines for interrupt handling).

### 7.4.4 The Software Architectural Design

The software architectural design shall be developed down to the level where the software units are identified.

### 7.4.5 The Software Architectural Design Shall Describe

- The static design aspects of the software architectural elements.
- The dynamic design aspects of the software architectural elements.

**NOTE 1**: Static design aspects address the software structure including its hierarchical levels, the data types and their characteristics, the external interfaces of the software components, the external interfaces of the embedded software, the global variables, and the constraints including the scope of the architecture and external dependencies.

**NOTE 2**: Dynamic design aspects address the functional chain of events and behavior, the logical sequence of data processing, the control flow and concurrency of processes, the data flow through interfaces and global variables, and the temporal constraints.

**NOTE 3**: To determine the dynamic behavior (e.g. of tasks, time slices and interrupts) the different operating states (e.g. power-up, shut-down, normal operation, calibration and diagnosis) are considered.

**NOTE 4**: To describe the dynamic behavior (e.g. of tasks, time slices and interrupts), the communication relationships and their allocation to the system hardware (e.g. CPU and communication channels) are specified.

### 7.4.6 The Software Safety Requirements

The software safety requirements shall be hierarchically allocated to the software components down to software units. As a result, each software component shall be developed in compliance with the highest ASIL of any of the requirements allocated to it.

**NOTE**: During this process of design development and requirement allocation, splitting or further refinement of software safety requirements in accordance with Clause 6 can be necessary.

### 7.4.7 If a Pre-existing Software Architectural Element

If a pre-existing software architectural element is used without modifications in order to meet the assigned safety requirements without being developed according to the ISO 26262 series of standards, then it shall be qualified in accordance with ISO 26262-8:2018, Clause 12.

**NOTE 1**: The use of qualified software components does not affect the applicability of Clauses 10 and 11. However, some activities described in Clauses 8 and 9 can be omitted.

**NOTE 2**: The suitability for reuse of software elements developed according to the ISO 26262 series of standards is performed during the verification of the software architectural design.

### 7.4.8 If the Embedded Software Has to Implement Software Components

If the embedded software has to implement software components of different ASILs, or safety-related and non-safety-related software components, then all of the embedded software shall be treated in accordance with the highest ASIL, unless the software components meet the criteria for coexistence in accordance with ISO 26262-9:2018, Clause 6.

### 7.4.9 If Software Partitioning

If software partitioning (see Annex D) is used to implement freedom from interference between software components it shall be ensured that:
- The shared resources are used in such a way that freedom from interference of software partitions is ensured.
- The software partitioning is supported by dedicated hardware features or equivalent means (this requirement applies to ASIL D, in accordance with 4.4).
- The element of the software that implements the software partitioning is developed in compliance with the highest ASIL assigned to any requirement of the software partitions.
- Evidence for the effectiveness of the software partitioning is generated during software integration and verification (in accordance with Clause 10).

### 7.4.10 Safety-oriented Analysis

Safety-oriented analysis shall be carried out at the software architectural level in accordance with ISO 26262-9:2018, Clause 8, in order to:
- Provide evidence for the suitability of the software to provide the specified safety-related functions and properties as required by the respective ASIL.
- Identify or confirm the safety-related parts of the software.
- Support the specification and verify the effectiveness of the safety measures.

**NOTE 1**: Safety-related properties include independence and freedom from interference requirements.

**NOTE 2**: Safety measures include safety mechanisms that have been derived from safety-oriented analyses and can cover issues associated with random hardware failures as well as software faults.

**NOTE 3**: See Annex E for additional information about the application of safety-oriented analysis at the software architectural level and the selection of appropriate safety measures.

### 7.4.11 If the Implementation of Software Safety Requirements

If the implementation of software safety requirements relies on freedom from interference or sufficient independence between software components, dependent failures and their effects shall be analysed in accordance with ISO 26262-9:2018, Clause 7.

**NOTE**: See Annex E and ISO 26262-9:2018, Annex C for additional information about the application of analyses of dependent failures at the software architectural level.

### 7.4.12 Depending on the Results of the Safety-oriented Analyses

Depending on the results of the safety-oriented analyses at the software architectural level in accordance with 7.4.10 or 7.4.11, safety mechanisms for error detection and error handling shall be applied.

**NOTE 1**: Annex E provides guidance for deciding if safety mechanisms are required for a failure mode.

**NOTE 2**: Safety mechanisms for error detection can include range checks of input and output data, plausibility check (e.g. using a reference model of the desired behavior, assertion checks, or comparing signals from different sources), monitoring of program execution by`

## 7.5 Work products

<treqs-element id="596e42d806e911efaea1a155b1602379" type="boundaryObject">
### 7.5.1 Software architectural design specification 
resulting from requirements 7.4.1 to 7.4.13.
</treqs-element>

<treqs-element id="79943a0e06e911efaea1a155b1602379" type="boundaryObject">
### 7.5.2 Safety analysis report 
resulting from requirement 7.4.10.
</treqs-element>
<treqs-element id="a5ea9dfa06e911efaea1a155b1602379" type="boundaryObject">
### 7.5.3 Dependent failures analysis report 
resulting from requirement 7.4.11.
</treqs-element>


### 7.5.4 Software verification report 
resulting from requirement 7.4.14.



# 8. Software Unit Design and Implementation

## 8.1 Objectives

The objectives of this sub-phase are:
- To develop a software unit design in accordance with the software architectural design, the design criteria, and the allocated software requirements which supports the implementation and verification of the software unit.
- To implement the software units as specified.

## 8.2 General

Based on the software architectural design, the detailed design of the software units is developed. The detailed design can be represented in the form of a model. The implementation at the source code level can be manually or automatically generated from the design in accordance with the software development environment. In order to develop a single software unit design, both software safety requirements and non-safety-related requirements are implemented. Hence, in this sub-phase, safety-related and non-safety-related requirements are handled within one development process.

## 8.3 Inputs to this Clause

### 8.3.1 Prerequisites

The following information shall be available:
- Documentation of the software development environment in accordance with 5.5.1.
- Hardware-software interface specification (refined) in accordance with 6.5.2.
- Software architectural design specification in accordance with 7.5.1.
- Software safety requirements specification in accordance with 6.5.1.
- Configuration data in accordance with C.5.3, if applicable.
- Calibration data in accordance with C.5.4, if applicable.

### 8.3.2 Further Supporting Information

The following information can be considered:
- Technical safety concept (see ISO 26262-4:2018, 6.5.2).
- System architectural design specification (see ISO 26262-4:2018, 6.5.3).
- Specification of non-safety-related functions and properties of the software (from an external source).
- Safety analysis report (see 7.5.2).
- Dependent failure analysis report (see 7.5.3).

## 8.4 Requirements and Recommendations

### 8.4.1 The Requirements of This Sub-Clause

The requirements of this sub-clause shall be complied with if the software unit is a safety-related element.

**NOTE**: “Safety-related” means that the unit implements safety requirements, or that the criteria for coexistence (see ISO 26262-9:2018, Clause 6) of the unit with other units are not satisfied.

### 8.4.2 The Software Unit Design and Implementation

The software unit design and implementation shall:
- Be suitable to satisfy the software requirements allocated to the software unit with the required ASIL.
- Be consistent with the software architectural design specification.
- Be consistent with the hardware-software interface specification, if applicable.

**EXAMPLE**: Consistency and integrity of the interfaces to other software units; correctness, accuracy, and timeliness of input or output data.

### 8.4.3 To Avoid Systematic Faults

To avoid systematic faults and to ensure that the software unit design achieves the following properties, the software unit design shall be described using the notations listed in Table 5.
- Consistency.
- Comprehensibility.
- Verifiability.
- Maintainability.

**Table 5 — Notations for software unit design**

| ASIL | Notations |
|------|-----------|
| ABCD | Natural language, Informal notations, Semi-formal notations |
| 1a   | Natural language |
| 1d   | Formal notations |
| 1b   | Informal notations |
| 1c   | Semi-formal notations |

**NOTE**: Natural language can complement the use of notations for example where some topics are more readily expressed in natural language or provide an explanation and rationale for decisions captured in the notations.

### 8.4.4 The Specification of the Software Units

The specification of the software units shall describe the functional behaviour and the internal design to the level of detail necessary for their implementation.

**EXAMPLE**: Internal design can include constraints on the use of registers and storage of data.

### 8.4.5 Design Principles for Software Unit Design and Implementation

Design principles for software unit design and implementation at the source code level as listed in Table 6 shall be applied to achieve the following properties:
- Correct order of execution of subprograms and functions within the software units, based on the software architectural design.
- Correctness of data flow and control flow between and within the software units.
- Readability and comprehensibility.
- Consistency of the interfaces between the software units.
- Simplicity.
- Robustness.
- Suitability for software modification.
- Verifiability.

**Table 6 — Design principles for software unit design and implementation**

| ASIL | Principle |
|------|----------|
| A    | One entry and one exit point in subprograms and functions |
| B    | No multiple use of variable names |
| C    | No dynamic objects or variables, or else online test during their creation |
| D    | Initialization of variables |

**NOTE**: In the case of model-based development with automatic code generation, the methods for representing the software unit design are applied to the model which serves as the basis for the code generation.

## 8.5 Work Products

<treqs-element id="00eaccac06ea11efaea1a155b1602379" type="boundaryObject">
### 8.5.1 Software Unit Design Specification

Software unit design specification resulting from requirements 8.4.2 to 8.4.5.

**NOTE**: In the case of model-based development, the implementation model and supporting descriptive documentation, using methods listed in Tables 5 and 6, specifies the software unit.
</treqs-element>

# 9. Software Unit Verification

## 9.1 Objectives

The objectives of this sub-phase are:
- To provide evidence that the software unit design satisfies the allocated software requirements and is suitable for the implementation.
- To provide evidence that the implemented software unit complies with the unit design and fulfills the allocated software requirements with the required ASIL.
- To verify that the defined safety measures resulting from safety-oriented analyses in accordance with 7.4.10 and 7.4.11 are properly implemented.
- To provide sufficient evidence that the software unit contains neither undesired functionalities nor undesired properties regarding functional safety.

## 9.2 General

The software unit design and the implemented software units are verified by using an appropriate combination of verification measures such as reviews, analyses, and testing.

## 9.3 Inputs to this Clause

### 9.3.1 Prerequisites

The following information shall be available:
- Hardware-software interface (HSI) specification (refined) in accordance with 6.5.2.
- Software architectural design specification in accordance with 7.5.1.
- Software unit design specification in accordance with 8.5.1.
- Software unit implementation in accordance with 8.5.2.
- Configuration data in accordance with C.5.3, if applicable.
- Calibration data in accordance with C.5.4, if applicable.
- Safety analysis report in accordance with 7.5.2.
- Documentation of the software development environment in accordance with 5.5.1.

### 9.3.2 Further Supporting Information

The following information can be considered:
- None.

## 9.4 Requirements and Recommendations

### 9.4.1 The Requirements of This Sub-Clause

The requirements of this sub-clause shall be complied with if the software unit is a safety-related element.

**NOTE 1**: “Safety-related element” according to ISO 26262-1 means that the unit implements safety requirements, or that the criteria for coexistence (see ISO 26262-9:2018, Clause 6) of the unit with other units are not satisfied.

**NOTE 2**: The requirements of this clause address safety-related software units; other software standards (see ISO 26262-2:2018, 5.4.5.1) can apply for verification of other software units.

### 9.4.2 The Software Unit Design and the Implemented Software Unit

The software unit design and the implemented software unit shall be verified in accordance with ISO 26262-8:2018, Clause 9 by applying an appropriate combination of methods according to Table 7 to provide evidence for:
- Compliance with the requirements regarding the unit design and implementation in accordance with Clause 8.
- The compliance of the source code with its design specification.
- Compliance with the specification of the hardware-software interface (in accordance with 6.4.4), if applicable.
- Sufficient resources to support their functionality and properties.
- Confidence in the absence of unintended functionality and properties.
- Implementation of the safety measures resulting from the safety-oriented analyses in accordance with 7.4.10 and 7.4.11.

**Table 7 — Methods for software unit verification**

| ASIL | Methods |
|------|----------|
| ABCD | Walk-through, Semi-formal verification, Pair-programming, Formal verification, Control flow analysis, Static analyses based on abstract interpretation, Inspection, Data flow analysis, Static code analysis, Interface test, Requirements-based test, Fault injection test, Resource usage evaluation, Back-to-back comparison test between model and code |

**NOTE 3**: For model-based software development, the corresponding parts of the implementation model also represent objects for the verification planning. Depending on the selected software development process the verification objects can be the code derived from this model, the model itself, or both.

### 9.4.3 Deriving Test Cases for Software Unit Testing

To enable the specification of appropriate test cases for the software unit testing in accordance with 9.4.2, test cases shall be derived using the methods as listed in Table 8.

**Table 8 — Methods for deriving test cases for software unit testing**

| ASIL | Methods |
|------|----------|
| ABCD | Analysis of requirements, Error guessing based on knowledge or experience, Generation and analysis of equivalence classes, Analysis of boundary values |

### 9.4.4 Evaluating the Completeness of Verification

To evaluate the completeness of verification and to provide evidence that the objectives for unit testing are adequately achieved, the coverage of requirements at the software unit level shall be determined and the structural coverage shall be measured in accordance with the metrics as listed in Table 9. If the achieved structural coverage is considered insufficient, either additional test cases shall be specified or a rationale based on other methods shall be provided.

**Table 9 — Structural coverage metrics at the software unit level**

| ASIL | Methods |
|------|----------|
| ABCD | Statement coverage, Branch coverage, BCD, MC/DC (Modified Condition/Decision Coverage) |

**NOTE 1**: No target value or a low target value for structural coverage without a rationale is considered insufficient.

### 9.4.5 The Test Environment for Software Unit Testing

The test environment for software unit testing shall be suitable for achieving the objectives of the unit testing considering the target environment. If the software unit testing is not carried out in the target environment, the differences in the source and object code, as well as the differences between the test environment and the target environment, shall be analysed in order to specify additional tests in the target environment during the subsequent test phases.

## 9.5 Work Products

### 9.5.1 Software Verification Specification
resulting from requirements 9.4.2 to 9.4.5.

### 9.5.2 Software Verification Report (refined) 
resulting from requirement 9.4.2.
