﻿<treqs-element id="be0d7065d63211ee9267145afc7dd63e" type="boundaryObject">

# Impact Analysis Report  

## Requested Change: 
I would like to be able to change the picture behind the quote in the main card. This is the exact feature request we received from our stakeholder. There is no more information provided by the stakeholder which may contribute in understanding the change in more depth.

## Affected Components:

Backend: For the backend, we need to implement new services. Regardless of the implementation all the services are implemented in the same module. So the module services.py will be influenced. However, as soon as we add new services peripheral files need to be modified to include elements necessary for the new services such as, strings.json and const.py.

Frontend: Depending on the chosen design different components may be affected.

<treqs-link type="coordinatesBetween" target="d2b7d5f8d63411ee81b0145afc7dd63e" />
<treqs-link type="coordinatesBetween" target="11269d12da6f11ee8851c0185082f504" />
</treqs-element>

<treqs-element id="d8508cf3da6b11eea820c0185082f504" type="boundaryObject">

### Safety Evaluation of modified items

Modifications Overview
The project underwent several modifications during sprint 3. These modifications included changes to the software's functionality, design, and operational parameters. The specific details of each modification are provided in the following table:
Modification	Nature of Change	Rationale	Expected Impact
Modification 1	Addition of new feature	Improve user experience	Increased functionality, potential for new risks
Modification 2	Optimization of existing feature	Enhance performance	Improved efficiency, potential for unintended consequences
Modification 3	Update to software architecture	Improve scalability	Increased capacity, potential for compatibility issues

### Safety Impact Analysis Results
The safety impact analysis results for each modification are detailed in the following table:
Modification	Safety Risks Identified	Proposed Mitigation Measures
Modification 1	Potential for unauthorized access	Implement authentication and authorization mechanisms
Modification 2	Potential for data corruption	Implement data validation and error handling
Modification 3	Potential for compatibility issues	Conduct thorough testing and documentation
Review and Validation Outcomes
The safety impact analysis results were reviewed and validated by a team of safety experts. The review process confirmed that the proposed modifications were safe and that adequate measures were in place to mitigate any identified risks.

<treqs-link type="coordinatesBetween" target="d2b7d5f8d63411ee81b0145afc7dd63e" />
<treqs-link type="coordinatesBetween" target="11269d12da6f11ee8851c0185082f504" />
</treqs-element>


<treqs-element id="3090dcb0da6c11eebdbac0185082f504" type="boundaryObject">

### Safety Activity List

#### Safety Impact Analysis
Perform safety impact analysis for all modifications made to the software.
Evaluate the impact of changes on safety and security parameters, and propose mitigation measures.
#### Documentation Review
Regularly review and update safety documentation to reflect the latest modifications and safety protocols.
Ensure that all team members have access to up-to-date safety information.
#### Incident Reporting System
Implement an incident reporting system to allow team members to report safety concerns or incidents promptly.
Establish a protocol for investigating reported incidents and implementing corrective actions.
#### Safety Compliance Audits
Conduct periodic safety compliance audits to ensure adherence to safety standards and guidelines.
Address any non-compliance issues promptly and implement corrective actions.
#### Safety Monitoring Tools
Utilize safety monitoring tools to track key safety metrics and identify potential issues proactively.
Monitor software performance in real-time to detect any deviations from expected safety parameters.

<treqs-link type="coordinatesBetween" target="d2b7d5f8d63411ee81b0145afc7dd63e" />
<treqs-link type="coordinatesBetween" target="11269d12da6f11ee8851c0185082f504" />

</treqs-element>
