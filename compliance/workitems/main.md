<treqs-element id="d2b7d5f8d63411ee81b0145afc7dd63e" type="methodologicalIsland">
# TEAM ALPHA 
</treqs-element>

<treqs-element id="11269d12da6f11ee8851c0185082f504" type="methodologicalIsland">
# TEAM GAMMA 
</treqs-element>

<treqs-element id="0a82ecd4d63311eeb0eb145afc7dd63e" type="governanceTeam">
# ORGANISATION
</treqs-element>


<treqs-element id="481d1fadd63311ee8edb145afc7dd63e" type="driver">
# Driver , Type : Process 
<treqs-link type="drives" target="d2b7d5f8d63411ee81b0145afc7dd63e" />
</treqs-element>

<treqs-element id="2e7d9bdcd63311eeae0c145afc7dd63e" type="role">
# SAFETY MANAGER

<treqs-link type="responsibleFor" target="be0d7065d63211ee9267145afc7dd63e" />
<treqs-link type="responsibleFor" target="d8508cf3da6b11eea820c0185082f504" />
<treqs-link type="responsibleFor" target="3090dcb0da6c11eebdbac0185082f504" />
<treqs-link type="responsibleFor" target="425ec612da6b11ee87a1c0185082f504" />
<treqs-link type="partOf" target="d2b7d5f8d63411ee81b0145afc7dd63e" />
<treqs-link type="partOf" target="0a82ecd4d63311eeb0eb145afc7dd63e" />

</treqs-element>

<treqs-element id="21e1616ada6d11ee8700c0185082f504" type="role">
# PROJECT MANAGER
<treqs-link type="impacts" target="be0d7065d63211ee9267145afc7dd63e" />
<treqs-link type="partOf" target="d2b7d5f8d63411ee81b0145afc7dd63e" />
<treqs-link type="partOf" target="0a82ecd4d63311eeb0eb145afc7dd63e" />

</treqs-element>


