﻿<treqs-element id="425ec612da6b11ee87a1c0185082f504" type="boundaryObject">
# Item Level Modification Report

## Modification Report for Feature Change X

### Requested Change:
 I would like to be able to change the picture behind the quote in the main card. This is the exact feature request we received from our stakeholder. There is no more information provided by the stakeholder which may contribute in understanding the change in more depth.

### Affected Components:

#### Backend: 
For the backend, we need to implement new services. Regardless of the implementation all the services are implemented in the same module. So the module services.py will be influenced. However, as soon as we add new services peripheral files need to be modified to include elements necessary for the new services such as, strings.json and const.py.

#### Frontend: 
Depending on the chosen design different components may be affected.


<treqs-link type="coordinatesBetween" target="d2b7d5f8d63411ee81b0145afc7dd63e" />
<treqs-link type="coordinatesBetween" target="11269d12da6f11ee8851c0185082f504" />

</treqs-element>
