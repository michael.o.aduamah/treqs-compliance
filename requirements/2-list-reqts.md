# TReqs List

In this section, we describe refined requirements for [Req2: List Treqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements). 

<treqs-element id="a0820e06-9614-11ea-bb37-0242ac130002" type="requirement" >

### Req-2.1: Parameters and default output of treqs list

    Usage: treqs list [OPTIONS] [FILENAME]...

  List treqs elements in the specified folder

Options:

```--type TEXT``` Limit action to specified treqs element type

```--uid TEXT``` Limit action to treqs element with specified id

```--outlinks / --no-outlinks``` Print outgoing tracelinks  [default: False]

```--inlinks / --no-inlinks``` Print incoming tracelinks  [default: False]

```--recursive BOOLEAN``` List treqs elements recursively in all subfolders.

```--verbose / --no-verbose``` Print verbose output instead of only the most important messages.  [default: False]

```--plantuml / --no-plantuml```Generates a PlantUML diagram from the treqs elements.  [default: no-plantuml]

```--help``` Show this message and exit.

If `type` is omitted, all treqs elements, regardless of type, will be returned.

If `uid` is omitted, all treqs elements, regardless of uid, will be returned.

The default value for `recursive` is `True`. Unless otherwise specified, treqs shall list elements recursively.

`FILENAME` can either provide a directory or a file. If `FILENAME` is omitted, treqs defaults to `.`, i.e. the current working directory.

**Links to:**    

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)
</treqs-link>

<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591">

- addresses: [REQ-CT-P1.4-1 Support upfront traceability](stakeholder-requirements/collaborative-traceability.md#req-ct-p14-1-support-upfront-traceability)
</treqs-link>

</treqs-element>
<treqs-element id="63ef8bfa76ae11ebb811cf2f044815f7" type="requirement">

### Req-2.2 Information listed by treqs list

When listing treqs elements, treqs shall list the following information as a markdown table:

  - the element type, 
  - the label (i.e. the first non-empty line of a treqs element),
  - the id of the element (the UID provided by the treqs create command), and 
  - the file and location (line number) where the element start tag is located. 

The file and the line number shall be displayed in the same column separated by a colon in order for IDEs to interpret it.

Example:

```treqs list ./requirements/2-list-reqts.md```

```
| UID | Type | Label | File:Line |
| :--- | :--- | :--- | :--- |
| a0820e06-9614-11ea-bb37-0242ac130002 | requirement | ### Req-2.1: Parameters and default output of treqs list | ./requirements/2-list-reqts.md:5 |
| 63ef8bfa76ae11ebb811cf2f044815f7 | requirement | ### Req-2.2 Information listed by treqs list | ./requirements/2-list-reqts.md:50 |
| 437f09c6-9613-11ea-bb37-0242ac130002 | requirement | ### Req-2.3 Filter by type  | ./requirements/2-list-reqts.md:89 |
| abc40962a23511eba9dca7925d1c5fe9 | information | Note that the type should usually be defined in the TTIM. treqs list does however not check for this to be the case. Use treqs check instead to make sure that all types are consistent with the TTIM. | ./requirements/2-list-reqts.md:108 |
| a0820b4a-9614-11ea-bb37-0242ac130002 | requirement | ### Req-2.4 Filter by ID | ./requirements/2-list-reqts.md:113 |
| bc89e02a76c811ebb811cf2f044815f7 | requirement | ### Req-2.5 List all elements in a file | ./requirements/2-list-reqts.md:133 |
| 638fa22e76c911ebb811cf2f044815f7 | requirement | ### Req-2.6 List treqs elements in a directory | ./requirements/2-list-reqts.md:148 |
| 1595ed20a27111eb8d3991dd3edc620a | requirement | ### Req-2.7 List outgoing tracelinks | ./requirements/2-list-reqts.md:165 |
| d9e68f9aa27b11eb8d3991dd3edc620a | requirement | ### Req-2.8 List incoming tracelinks | ./requirements/2-list-reqts.md:218 |
```

**Links to:**

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)
</treqs-link>
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />

</treqs-element>

<treqs-element id="437f09c6-9613-11ea-bb37-0242ac130002" type="requirement" >

### Req-2.3 Filter by type 
TReqs shall allow to list treqs elements within a specified folder/file that have a specific type (e.g. requirement, test, quality requirement).

**Links to:**

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)
</treqs-link>
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />

Example:

```treqs list --type=requirement folderName```

</treqs-element>

<treqs-element id="abc40962a23511eba9dca7925d1c5fe9" type="information">
Note that the type should usually be defined in the TTIM. treqs list does however not check for this to be the case. Use treqs check instead to make sure that all types are consistent with the TTIM.
</treqs-element>


<treqs-element id="a0820b4a-9614-11ea-bb37-0242ac130002" type="requirement" >

### Req-2.4 Filter by ID
TReqs shall allow to list treqs elements within a specified folder/file that have a specific ID.

**Links to:**

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)
</treqs-link>
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />
<treqs-link type="addresses" target="54a4e59a9d3311ebb4d2c4b301c00591" />

Example:

```treqs list --uid "35590bca-960f-11ea-bb37-0242ac130002" folderName```

</treqs-element>

<treqs-element id="bc89e02a76c811ebb811cf2f044815f7" type="requirement">

### Req-2.5 List all elements in a file

If a file is given, treqs will list all treqs elements in that file.

**Links to:**

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)
</treqs-link>
<treqs-link type="relatesTo" target="a0820e06-9614-11ea-bb37-0242ac130002" />
</treqs-element>

<treqs-element id="638fa22e76c911ebb811cf2f044815f7" type="requirement">


### Req-2.6 List treqs elements in a directory

If a directory is given, treqs will list all treqs elements in all files of this directory. 
If the recursive parameter is given, treqs will list also all treqs elements in all subdirectories of the given directory.

**Links to:**

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)
</treqs-link>
<treqs-link type="relatesTo" target="bc89e02a76c811ebb811cf2f044815f7" />
</treqs-element>

<treqs-element id="1595ed20a27111eb8d3991dd3edc620a" type="requirement">

### Req-2.7 List outgoing tracelinks

Optionally, treqs list shall list the outgoing tracelinks of each treqs element. This list shall be integrated in the table of the list command. Labels and locations (filename and line number) of target treqs-elements shall be shown, if available (i.e., the current list command includes the file in which the target is specified).

The following command will result in the following output, since the parent of this requirement is specified in a different file.

Example:

```treqs list --outlinks --uid "1595ed20a27111eb8d3991dd3edc620a" ./requirements/2-list-reqts.md```

```
| UID | Type | Label | File:Line |
| :--- | :--- | :--- | :--- |
| 1595ed20a27111eb8d3991dd3edc620a | requirement | ### Req-2.7 List outgoing tracelinks | ./requirements/2-list-reqts.md:167 |
| --outlink--> (35590bca-960f-11ea-bb37-0242ac130002) | hasParent | Target: Target treqs element not found. Has the containing file been included in the scope? | -- |
| --outlink--> (63ef8bfa76ae11ebb811cf2f044815f7) | relatesTo | Target: ### Req-2.2 Information listed by treqs list | ./requirements/2-list-reqts.md:50 |
| --outlink--> (d9e68f9aa27b11eb8d3991dd3edc620a) | relatesTo | Target: ### Req-2.8 List incoming tracelinks | ./requirements/2-list-reqts.md:220 |
| --outlink--> (1e9885f69d3311eb859fc4b301c00591) | addresses | Target: Target treqs element not found. Has the containing file been included in the scope? | -- |
| --outlink--> (54a4e59a9d3311ebb4d2c4b301c00591) | addresses | Target: Target treqs element not found. Has the containing file been included in the scope? | -- |
```

at the same time, the following list command includes all requirements and therefore produces the desired result:

```treqs list --outlinks --uid "1595ed20a27111eb8d3991dd3edc620a" ./requirements```

```
| UID | Type | Label | File:Line |
| :--- | :--- | :--- | :--- |
| 1595ed20a27111eb8d3991dd3edc620a | requirement | ### Req-2.7 List outgoing tracelinks | requirements/2-list-reqts.md:172 |
| --outlink--> (35590bca-960f-11ea-bb37-0242ac130002) | hasParent | Target: ## Req-2: List TReqs Elements | requirements/treqs-system-requirements.md:113 |
| --outlink--> (63ef8bfa76ae11ebb811cf2f044815f7) | relatesTo | Target: ### Req-2.2 Information listed by treqs list | requirements/2-list-reqts.md:50 |
| --outlink--> (d9e68f9aa27b11eb8d3991dd3edc620a) | relatesTo | Target: ### Req-2.8 List incoming tracelinks | requirements/2-list-reqts.md:224 |
| --outlink--> (1e9885f69d3311eb859fc4b301c00591) | addresses | Target: #### REQ-CT-P1.4-1 Support upfront traceability | requirements/stakeholder-requirements/collaborative-traceability.md:72 |
| --outlink--> (54a4e59a9d3311ebb4d2c4b301c00591) | addresses | Target: #### REQ-CT-P1.4-2 Support tracelink maintenance | requirements/stakeholder-requirements/collaborative-traceability.md:81 |
```

**Links to:**

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)
</treqs-link>
<treqs-link type="relatesTo" target="63ef8bfa76ae11ebb811cf2f044815f7" />
<treqs-link type="relatesTo" target="d9e68f9aa27b11eb8d3991dd3edc620a" />
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />
<treqs-link type="addresses" target="54a4e59a9d3311ebb4d2c4b301c00591" />

</treqs-element>

<treqs-element id="d9e68f9aa27b11eb8d3991dd3edc620a" type="requirement">

### Req-2.8 List incoming tracelinks

Optionally, treqs list shall list the incoming tracelinks of each treqs element. This list shall be integrated in the table of the list command. Labels and locations (filename and line number) of target treqs elements shall be shown, if available (i.e. the current list command includes the file in which the target is specified).

Example:

```treqs list --inlinks --uid "d9e68f9aa27b11eb8d3991dd3edc620a" ./requirements```

```
| UID | Type | Label | File:Line |
| :--- | :--- | :--- | :--- |
| d9e68f9aa27b11eb8d3991dd3edc620a | requirement | ### Req-2.8 List incoming tracelinks | requirements/2-list-reqts.md:224 |
| --inlink--> (1595ed20a27111eb8d3991dd3edc620a) | relatesTo | Source: ### Req-2.7 List outgoing tracelinks | requirements/2-list-reqts.md:172 |
```

**Links to:**

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)
</treqs-link>
<treqs-link type="relatesTo" target="63ef8bfa76ae11ebb811cf2f044815f7" />
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />
<treqs-link type="addresses" target="54a4e59a9d3311ebb4d2c4b301c00591" />

</treqs-element>

<treqs-element id="6473290ed22e11eeadb88de628732e03" type="requirement">

### Req-2.9 Generate PlantUML diagram

Optionally, `treqs list` shall print out its output in PlantUML format, instead
of the default Markdown-table format. The PlantUML should be valid and should
contain the exact same information in the corressponding `treqs list` i.e. any
of the options that can be used for `treqs list` must be preserved and used in
the generation of the PlantUML information.

Example:
```treqs list --inlinks --uid "d9e68f9aa27b11eb8d3991dd3edc620a" ./requirements```

```
@startuml
map "**### Req-2.8 List incoming tracelinks**" as
d9e68f9aa27b11eb8d3991dd3edc620a {
uid => ""d9e68f9aa27b11eb8d3991dd3edc620a""
type => //requirement//
location => requirements/\n2-list-reqts.md:222

}
map "**### Req-2.7 List outgoing tracelinks**" as
1595ed20a27111eb8d3991dd3edc620a {
uid => ""1595ed20a27111eb8d3991dd3edc620a""
type => //requirement//
location => requirements/\n2-list-reqts.md:169

}
1595ed20a27111eb8d3991dd3edc620a --> d9e68f9aa27b11eb8d3991dd3edc620a :
relatesTo
@enduml
```
**Links to:**

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002">

- hasParent: [Req-2: List TReqs Elements](treqs-system-requirements.md#req-2-list-treqs-elements)
</treqs-link>
<treqs-link type="relatesTo" target="63ef8bfa76ae11ebb811cf2f044815f7" />
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />
<treqs-link type="addresses" target="54a4e59a9d3311ebb4d2c4b301c00591" />

</treqs-element>
