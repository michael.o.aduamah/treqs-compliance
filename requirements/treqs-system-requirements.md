<treqs>

# TReqs requirements

## Conventions


<treqs-element id="51928136509311edbe22c9328ceec9a7" type="requirement">

### Conv-1: TReqs shall work with requirements in markdown syntax.

</treqs-element>

<treqs-element id="ccae17ea509311edbe22c9328ceec9a7" type="information">

Info: Managing requirements in textfiles allows to rely on git (or: similar) for collaboration. 

<treqs-link type="relatesTo" target="51928136509311edbe22c9328ceec9a7" />
</treqs-element>


<treqs-element id="571a0556509411edbe22c9328ceec9a7" type="requirement">

### Conv-2: TReqs shall support tooling and scripting by providing metadata as xml elements and attributes.

</treqs-element>

<treqs-element id="95a1e30c509411edbe22c9328ceec9a7" type="information">

Info: This file provides an example of the convention of mixing markdown and xml syntax.
<treqs-link type="relatesTo" target="51928136509311edbe22c9328ceec9a7" />
<treqs-link type="relatesTo" target="571a0556509411edbe22c9328ceec9a7" />

</treqs-element>

<treqs-element id="e21d72f0509411edbe22c9328ceec9a7" type="requirement">

### Conv-3: The first none-empty textline of a treqs element is regarded as its label. 

</treqs-element>
<treqs-element id="e21d72f1509411edbe22c9328ceec9a7" type="information">

Info: Since the first non-empty line is considered as a label, it should be a short characterisation of the requirement, allowing to identify the requirement in context, but still fit easily in one line on the screen.
This allows for example to have meaningful listings of requirements instead of just representing them as uids.
<treqs-link type="relatesTo" target="e21d72f0509411edbe22c9328ceec9a7" />

</treqs-element>

<treqs-element id="001bb234509611edbe22c9328ceec9a7" type="requirement">

### Conv-4: TReqs assumes the label to be a markdown heading (but will also work if this is not the case)

</treqs-element>
<treqs-element id="001bb235509611edbe22c9328ceec9a7" type="information">

Info: A markdown heading is serving as an html anchor. 
This allows to automatically create links to a particular requirement which can be interpreted by any markdown renderer.

<treqs-link type="relatesTo" target="001bb234509611edbe22c9328ceec9a7" />
</treqs-element>

<treqs-element id="e76995c4509c11edbe22c9328ceec9a7" type="requirement">

### Conv-5: TReqs stores configuration in the project it is used

- A configuration file `ttim.yaml` is used to store the treqs Type and Tracelink information model. This allows users to define treqs-element types and their links for each project individually.
- A folder with templates as markdown files can be maintained per project and allows to create new treqs elements accordingly. If this does not exist, treqs falls back to its default templates.

</treqs-element>

<treqs-element id="8793c8ca96eb11ebaca5c4b301c00591" type="requirement">

### Conv-6: MIT License

TReqs shall be available under MIT license. It is therefore preferred, that libraries that we use are also using MIT license. 
</treqs-element>

<treqs-element id="97a8fb92-9613-11ea-bb37-0242ac130002" type="requirement" >

## Req-1: Create TReqs Elements
TReqs shall be able to create treqs-elements 

**Elements that link here:**
- hasParent: 
  - [Req-1.1: TReqs create shall provide the following command line interface](1-create-reqts.md#req-11-treqs-create-shall-provide-the-following-command-line-interface)
  - [Req-1.2: Unique IDs](1-create-reqts.md#req-12-unique-ids)
  - [Req-1.3: TReqs create shall be non-interactive by default](1-create-reqts.md#req-13-treqs-create-shall-be-non-interactive-by-default)
  - [Req-1.4: TReqs create shall have an interactive mode](1-create-reqts.md#req-14-treqs-create-shall-have-an-interactive-mode)
  - [Req-1.5: TReqs create shall allow the use of templates](1-create-reqts.md#req-15-treqs-create-shall-allow-the-use-of-templates)
  - [Req-1.6: TReqs create shall allow to create more than one element at a time](1-create-reqts.md#req-16-treqs-create-shall-allow-to-create-more-than-one-element-at-a-time)

</treqs-element>
<treqs-element id="d17370d8509611edbe22c9328ceec9a7" type="information">

**Examples (Req-1):**

This minimal example returns a treqs-element with a newly generated id of the specified type and prints it on commandline:

    treqs create --type=requirement

<treqs-link type="relatesTo" target="97a8fb92-9613-11ea-bb37-0242ac130002" />
</treqs-element>
<treqs-element id="d17370d9509611edbe22c9328ceec9a7" type="information">

**Potential solutions (Req-1):**

- Have a function that returns text to the terminal/console, which then can be copied and pasted into a markdown file or header of an automated test
- ~~Allow to specify a file to which the new element should be appended~~
- Such a function could take optional paramenters, such as type and test. If those are missing, just an empty treqs-element structure is returned.
<treqs-link type="relatesTo" target="97a8fb92-9613-11ea-bb37-0242ac130002" />
</treqs-element>

<treqs-element id="35590bca-960f-11ea-bb37-0242ac130002" type="requirement" >

## Req-2: List TReqs Elements
TReqs shall be able to list treqs-elements within a specified gitlab project.

**Elements that link here:**
- hasParent
  - [Req-2.1: Parameters and default output of treqs list](2-list-reqts.md#req-21-parameters-and-default-output-of-treqs-list)
  - [Req-2.2 Information listed by treqs list](2-list-reqts.md#req-22-information-listed-by-treqs-list)
  - [Req-2.3 Filter by typ](2-list-reqts.md#req-23-filter-by-type)
  - [Req-2.4 Filter by ID](2-list-reqts.md#req-24-filter-by-id)
  - [Req-2.5 List all elements in a file](2-list-reqts.md#req-25-list-all-elements-in-a-file)
  - [Req-2.6 List treqs elements in a directory](2-list-reqts.md#req-26-list-treqs-elements-in-a-directory)
  - [Req-2.7 List outgoing tracelinks](2-list-reqts.md#req-27-list-outgoing-tracelinks)
  - [Req-2.8 List incoming tracelinks](2-list-reqts.md#req-28-list-incoming-tracelinks)


</treqs-element>
<treqs-element id="8d01efc8509711edbe22c9328ceec9a7" type="information">

**Example (Req-2):**

    treqs list

<treqs-link type="relatesTo" target="35590bca-960f-11ea-bb37-0242ac130002" />

</treqs-element>

<treqs-element id="c153021087f011eb8a15c4b301c00591" type="requirement">

## Req-4: Process treqs files

TReqs shall be able to process files in the following way:

- Create and link image from embedded PlantUML

**Elements that link here:**

- hasParent
  - [Req-4.1 Process plantuml models](4-process-elements.md#req-41-process-plantuml-models)
  - [Req-4.5: Allow for extensions in treqs process](4-process-elements.md#req-45-allow-for-extensions-in-treqs-process)

</treqs-element>


<treqs-element id="8cedc2a4509d11edbe22c9328ceec9a7" type="information">

**Example (Req-4):**

```
treqs process filename
```


</treqs-element>
<treqs-element id="8cedc2a5509d11edbe22c9328ceec9a7" type="requirement">

## Req-5: Check types and tracelinks

TReqs shall allow to check whether requirements are well-formed with respect to the defined types and tracelinks.

<treqs-link type="relatesTo" target="e76995c4509c11edbe22c9328ceec9a7" />

**Elements that link here:**

- hasParent
  - [Req-5.1 Parameters and default output of treqs check](5-check-reqts.md#req-51-parameters-and-default-output-of-treqs-check)
  - [Req-5.2 Check for unrecognized or empty/missing types.](5-check-reqts.md#req-52-check-for-unrecognized-or-emptymissing-types)
  - [Req-5.3 Check for unrecognized or missing link types.](5-check-reqts.md#req-53-check-for-unrecognized-or-missing-link-types)
  - [Req-5.4 Check for missing links.](5-check-reqts.md#req-54-check-for-missing-links)
  - [Req-5.5: Check missing ids.](5-check-reqts.md#req-55-check-missing-ids)
  - [Req-5.6: Check duplicate ids.](5-check-reqts.md#req-56-check-duplicate-ids)
  - [Req-5.7: Check for non-existent referenced elements.](5-check-reqts.md#req-57-check-for-non-existent-referenced-elements)
  - [Req-5.8 Check for wrong link types.](5-check-reqts.md#req-58-check-for-wrong-link-types)
</treqs-element>


----



# A few more rough thoughts

## consistency aspects

- The functions specified above would create a common infrastructure. In addition, it should be possible to get all tracelinks to and from a treqs-element.
- In addition, it should be possible to store within a gitlab project 
   - a requirements information model (i.e. which treqs-elements are allowed in this gitlab-project) and 
   - a traceability information model (i.e. which types of links are allowed/required between treqs-elements)
- since that might be difficult, we might go for a different approach
   - since the convenience functions above provide good infrastructure, the gitlab project could store instead consistency rules
   - such consistency rules are written in python and executed by the treqs command
   - a rule can _fire_, i.e. add a line to a log consisting of criticality, location, message, description (including suggestions for fixing)
   - rules could for example check whether mandatory tracelinks exist. 
   - they could also check certain conventions or writing rules (e.g. avoiding weakwords, passive voice, ...)
   
It should be a good practice to check consistency before pushing, thus, these rules resemble unit tests.
   
## Replacement rules

In addition to consistency, we have a lot of things where treqs should add automatically generated stuff to a markdown file.

- Mebrahtom's approach to modeling, where a script generates a line that calls the plantuml webservice for generating a picture of each plantuml diagram
- One could generate some markdown text with links to elements

Those should, of course, be marked somehow, since the next execution should overwrite them. 

It is conceivable to automatically execute those on commit or push.


</treqs>
