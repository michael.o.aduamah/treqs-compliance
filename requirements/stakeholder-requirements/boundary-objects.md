<treqs>
# TReqs and Boundary Objects

We see treqs as a mechanism to facilitate communication across teams and disciplines. 
The BOMI (Boundary Objects and Methodological Islands) theory therefore appears to be a good fit to analyse how treqs can best fulfil this role.


## 1. Boundary Objects

<treqs-element id="d2f34b420ff411ecbb17c4b301c00591" type="stakeholder-need">

### 1.1 System Requirements as Boundary Object

TReqs shall supports the management of system requirements together with the code they represent.
</treqs-element>
We derive the following core considerations:

<treqs-element id="1ed57b2a0ff511ec8f07c4b301c00591" type="stakeholder-requirement">

#### REQ-BO-1 Support managing system requirements by agile teams in iterations

As a developer in an agile team, I want to manage requirements in a natural format that is useful during iterative work so that changes are implicitly traced to changes on code and tests.


<treqs-link type="addresses" target="d2f34b420ff411ecbb17c4b301c00591" />
</treqs-element>
<treqs-element id="beebefee100f11ec9002c4b301c00591" type="stakeholder-requirement">

#### REQ-BO-2 Support just-in-time updates of requirements and change propagation

As a developer, I want to update requirements during a sprint just-in-time and to ensure that knowledge of the update reaches those who need to know so that requirements are kept useful to describe the system as it currently is.
<treqs-link type="addresses" target="d2f34b420ff411ecbb17c4b301c00591" />
</treqs-element>

<treqs-element id="569ace96101011ecad9fc4b301c00591" type="information">

Comment: In contrast to other RE tools, this means that the system requirements should relate to things that the developers are working on. 
System level views can then be generated, but the text file should be useful for teams. 
It appears to us, that organizing requirements per component is most useful here, even for feature teams. 


Example: requirements in treqs are in one file per component. 
All requirements for ``treqs list`` are kept together in one file, all requirements for ``treqs check`` are kept in a separate file. 
This makes it easy to get an overview when starting to developing on a particular component.

<treqs-link type="relatesTo" target="1ed57b2a0ff511ec8f07c4b301c00591" />
<treqs-link type="relatesTo" target="beebefee100f11ec9002c4b301c00591" />
</treqs-element>

<treqs-element id="326046ba101411ec86d9c4b301c00591" type="stakeholder-need">

### 1.2 Stakeholder Requirements as Boundary Object


The system requirements themselves cannot exist in a vacuum. 
In order to allow agile teams to prioritize, they must be connected to a stakeholder need or business case. 
Stakeholder Requirements are a natural way of breaking them down.
</treqs-element>

<treqs-element id="c2a85482101511ec992ec4b301c00591" type="stakeholder-requirement">

#### REQ-BO-3 Traceability between stakeholder and system requirements

As a system manager, I want to ensure that each stakeholder-requirement is traced to from a system requirement 
and that each system requirement traces directly or indirectly to a system requirement 
so that it is explicit that all documented stakeholder needs are addressed and that all system requirements are needed by stakeholders.
<treqs-link type="addresses" target="326046ba101411ec86d9c4b301c00591" />
</treqs-element>
<treqs-element id="d41ffd46101511ecbecac4b301c00591" type="stakeholder-requirement">

#### REQ-BO-4 Stakeholder requirements managed in external tool

As a system manager I want to maintain stakeholder requirements in an external tool so that customer facing roles feel more comfortable with maintaining them.
Thus, treqs must support tracing to external tools.
<treqs-link type="addresses" target="326046ba101411ec86d9c4b301c00591" />
</treqs-element>
<treqs-element id="e2d2d872101511ecb037c4b301c00591" type="stakeholder-requirement">

#### REQ-BO-5 Stakeholder requirements organized independently from system requirements
As a system manager I do not want to constraint the organization of stakeholder requirements in any way so that they can be effectively managed and evolved.
Stakeholder requirements are likely better organized with respect to particular stakeholder groups and treqs should allow to present them in relation to system requirements when needed.
<treqs-link type="addresses" target="326046ba101411ec86d9c4b301c00591" />
</treqs-element>

<treqs-element id="710d2a14101811ec9e46c4b301c00591" type="stakeholder-need">

### 1.3 Requirements/Traceability Information Model as Boundary Object
The requirements and traceability information model is a boundary object that allows several teams as well as system engineering roles to align on a structure for requirements and tracing. 
</treqs-element>

<treqs-element id="0c4f3b2a101911ecaa7bc4b301c00591" type="stakeholder-requirement">

#### REQ-BO-6 Explicit representation of requirements and traceability information model
As a senior requirements engineer, I want to explicitly document a requirements and traceability information model 
so that it is easily available to all its stakeholders.

<treqs-link type="addresses" target="710d2a14101811ec9e46c4b301c00591" />
<treqs-link type="relatesTo" target="7438f4c69d3011ebb63fc4b301c00591" />
</treqs-element>
<treqs-element id="0cf77fb0101911ecb635c4b301c00591" type="stakeholder-requirement">

#### REQ-BO-7 Evolution of requirements and traceability information model

As a senior requirements engineer, I want to support and manage the evolution of the requirements and traceability information model 
so that it supports the changing needs of its stakeholders.
<treqs-link type="addresses" target="710d2a14101811ec9e46c4b301c00591" />
</treqs-element>
<treqs-element id="0d91c6ba101911ecb6b8c4b301c00591" type="stakeholder-requirement">

#### REQ-BO-8 Balange alignment and diversity of requirements and traceability information model

As a manager I want to enable different projects and teams to use specialized and differing requirements and traceability information models
so that their specific needs are optimally supported.
<treqs-link type="addresses" target="710d2a14101811ec9e46c4b301c00591" />
<treqs-link type="addresses" target="c3eb064a101b11ec9e9bc4b301c00591" />
</treqs-element>



## 2. Methodological Islands

<treqs-element id="83c6a8a8101b11ecad0fc4b301c00591" type="stakeholder-need">

### 2.1 Support Development Teams

Each team can be seen as a MI, since they have some autonomy in agile methods.
Thus, it must be allowed to them to create their own culture, ways of working, and practices with reasonable limits.
</treqs-element>

<treqs-element id="c3362004101b11ec9c22c4b301c00591" type="stakeholder-requirement">

#### REQ-BO-9 Support alignment of teams through peer-reviews

As a system manager, I want to enable peer-reviews of (system) requirements across teams so that alignment and high quality of requirements is enabled.
<treqs-link type="addresses" target="83c6a8a8101b11ecad0fc4b301c00591" />
</treqs-element>


<treqs-element id="c3eb064a101b11ec9e9bc4b301c00591" type="stakeholder-need">

### 2.2 Support System Level Roles

On system level, stakeholders such as system managers and testers have a different perspective on the system than development teams.
</treqs-element>

<treqs-element id="97b5d95a101c11ec8aadc4b301c00591" type="stakeholder-requirement">

#### REQ-BO-10 Support alignment between development level and system level views
As a system manager I want to create awareness about changing system requirements 
so that development level and system level roles are continuously aligned.
<treqs-link type="addresses" target="c3eb064a101b11ec9e9bc4b301c00591" />
</treqs-element>

<treqs-element id="0867ccce101f11ec942dc4b301c00591" type="stakeholder-need">

### 2.3 Support Customer Facing Roles

Stakeholder requirements and system requirements must be kept aligned. 
The responsibility for that is a bit unclear in literature, it is spread between POs and Dev:

- the stakeholder requirements are usually in the hand of the PO
- the system requirements should be in the hand of the dev teams

</treqs-element>
<treqs-element id="0a26f21a101f11eca5bcc4b301c00591" type="stakeholder-requirement">

#### REQ-BO-11 Tracelinks as a boundary object between customer facing and development roles


As a senior requirements engineer I want to enable joint governance of tracelinks between stakeholder and system requirements 
so that customer facing roles (i.e. PO) and development level roles are aligned.
 Note: In this way, the tracelinks are a shared resource (boundary object).
<treqs-link type="addresses" target="0867ccce101f11ec942dc4b301c00591" />
</treqs-element>
</treqs>
