import logging
import os
import yaml
from typing import List
from treqs.treqs_element import *
from treqs.list_elements import list_elements
from reportlab.lib.pagesizes import landscape, legal
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet


class check_compliance:
    def __init__(self):
        self.__logger = logging.getLogger('treqs-on-git.treqs-ng')
        self.__list_items = list_elements()
        self.treqs_element_factory = treqs_element_factory()
        self.work_items= []
        self.compliance_results = []
        self.standards_list =[]
        self.standards_list_path =str
        self.required_work_items = []
        self.required_work_item_uids=[]
        self.roles_responsible=[]
        self.roles_responsible_uids=[]
        self.processed_targets = []
        self.selected_standard_part = str
        self.compliance_percentage = str

        

    def check_compliance(self,recursive,ttim,standards,verbose):


        self.standards_list_path = standards
        # Extract the names of available defined standards from standards.yaml
        self.standards_list = self.load_standards(standards)

        # Prompt user to select standard
        self.selected_standard = self.prompt_standard_selection()

         # Trace required work items in project directory
        self.trace_requirements(self.selected_standard)

        # Log compliance check results
        self.log_compliance_report()

        # Export compliance check results
        self.prompt_options()
  
    def load_standards(self, file_path):
            if os.path.isfile(file_path):
                with open(file_path, 'r') as yaml_file:
                    yaml_data = yaml.safe_load(yaml_file)

            standards_data = yaml_data.get('standards', [])

            # Extract the names of available standards
            for standard in standards_data:
                self.standards_list.append(standard['name'])
        
            return self.standards_list 
    
    
    
    def prompt_standard_selection(self):
        
            # Display the list of standards
            print("\n Please select a standard:")
            for i, standard in enumerate(self.standards_list, start=1):
                print(f"{i}. {standard}")

            # Prompt the user for selection
            selection = input("\n Enter the number assigned to the standard you wish to check compliance against: ")
            
            # Validate the input and return the selected standard
            try:
                selection_index = int(selection) - 1
                if 0 <= selection_index < len(self.standards_list):
                    return self.standards_list[selection_index]
                else:
                    self.__logger.log(30, "Invalid selection. Please try again.")
                    return self.prompt_standard_selection()
            except ValueError:
                self.__logger.log(30, "Invalid input. Please enter a number.")
                return self.prompt_standard_selection()
    
    def trace_requirements(self,selected_standard):
         #Load treqs elements (required artifacts) from the standard definition
         standard_directory = self.load_standard_dir(selected_standard)

         self.selected_standard_part = self.load_standard_dir(selected_standard,part=True)
         # Fetch all treqs elements from the required work items
         self.required_work_items = self.__list_items.get_element_list(standard_directory,treqs_type="boundaryObject")

         # Extract UIDs of required work items
         self.required_work_item_uids = [item.uid for item in self.required_work_items]


         # Fetch all roles with outlinks to the required work items in the selected standard directory
         self.roles_responsible = self.__list_items.get_element_list(standard_directory, treqs_type="role", outlinks=True)
         self.roles_responsible_uids = [item.uid for item in self.roles_responsible if item.uid not in self.required_work_item_uids]
         
         # Check for fullfiled standards' artifacts
         if self.required_work_item_uids:
            self.append_compliant()
         else:
             pass
             

         # Calculate compliance percentage
         if self.processed_targets and self.required_work_item_uids:
            self.compliance_percentage = int(round(self.calculate_compliance_percentage(self.processed_targets, self.required_work_item_uids)))
         else:
            self.compliance_percentage = 0  

         # Check for unfulfilled standards' artifacts
         if self.required_work_item_uids:
            self.append_non_compliant(self.processed_targets)
         else:
            pass  
           
         # Check for required roles for each artifact
         self.append_roles()



         return
    
    #check through the selected standard's directory 
    def load_standard_dir(self, selected_standard,part=False):
         if os.path.isfile(self.standards_list_path):
            with open(self.standards_list_path, 'r') as yaml_file:
                yaml_data = yaml.safe_load(yaml_file)

         standards_data = yaml_data.get('standards', [])

         # Extract the names of available standards' parts
         for standard in standards_data:
            if standard['name'] == selected_standard:
                return standard['part'] if part else standard['filePath']
            
            return None 
         
    def append_compliant(self):
        #fetch all work artifacts tagged as boundary objects within the project directory
        self.all_work_items = self.__list_items.get_element_list(None,treqs_type='boundaryObject',outlinks=True)

        for element in self.all_work_items:
            
            #fetch all items with outlinks to the selected standard's required work items
            filtered_outlinks = [tl for tl in element.outlinks if tl.target in self.required_work_item_uids and tl.source not in self.roles_responsible_uids]

            for tl in filtered_outlinks:
                source_te = self.treqs_element_factory.get_element_with_uid(tl.source)
                target_te = self.treqs_element_factory.get_element_with_uid(tl.target)
                label = source_te.label
                file_line = "{file}:{line}".format(file=source_te.file_name, line=source_te.placement)
                file_line2 = "{file}:{line}".format(file=target_te.file_name, line=target_te.placement)
              
               
                compliance_result = {
                    'Standard': self.selected_standard, 
                    'Part': self.selected_standard_part, 
                    'Required Work Item': target_te.label, 
                    'Item Uid': tl.target,
                    'Location1': file_line2,
                    'Fulfilled?': 'YES', 
                    'Location2': file_line, 
                    'Role Responsible': "Not found"
                }

                self.compliance_results.append(compliance_result)
                self.processed_targets.append(tl.target)


        return None
    
    def append_non_compliant(self, processed_targets):

        #remove already fulfilled artifacts from the required work item list
        for target in processed_targets:
            if target in self.required_work_item_uids:
                self.required_work_item_uids.remove(target)
            
        for workitem in self.required_work_item_uids:
                elem = self.treqs_element_factory.get_element_with_uid(workitem)
                label = elem.label
                file_line = "{file}:{line}".format(file=elem.file_name, line=elem.placement)

                compliance_result = {
                    'Standard': self.selected_standard, 
                    'Part': self.selected_standard_part, 
                    'Required Work Item': label,
                    'Item Uid': workitem,
                    'Location1': file_line, 
                    'Fulfilled?': 'NO', 
                    'Location2': 'Not found', 
                    'Role Responsible': "Not found"
                }
                
                if not any(d == compliance_result for d in self.compliance_results):
                    self.compliance_results.append(compliance_result)
            
                processed_targets.append(workitem)

        return None
    
    def append_roles(self):
         for result in self.compliance_results:
            uid = result['Item Uid']

            for role_uid in self.roles_responsible_uids:
                role_req = self.treqs_element_factory.get_element_with_uid(role_uid)
                links = [link for link in role_req.outlinks if link.target == uid]
                if links:
                     for link in links:
                         role_e = self.treqs_element_factory.get_element_with_uid(link.source)
                         role_label = role_e.label

                         # Find the index of the current result in self.compliance_results
                         index = next((index for (index, d) in enumerate(self.compliance_results) if d["Item Uid"] == uid), -1)

                         # Update the "Role Responsible" field for the matching workitem
                         if index != -1:
                            self.compliance_results[index]["Role Responsible"] = role_label

         return None
    
    def calculate_compliance_percentage(self,workitemsDone,workitemsNotDone):
        common_elements = set(workitemsDone).intersection(set(workitemsNotDone))
        num_common_elements = len(common_elements)

        total_elements = set(workitemsDone).union(set(workitemsNotDone))
        num_total_elements = len(total_elements)
        
        if num_total_elements == 0:
         return "Cannot calculate compliance percentage. Division by zero."

        percentage = (num_common_elements / num_total_elements) * 100

        return percentage
    
    def log_compliance_report(self):
        self.__logger.log(20, '\n\n -------------------------------------------------------------------------------------------------------------')
        self.__logger.log(20, 'COMPLIANCE CHECK RESULTS TO %s- %s %% COMPLETE.      ', self.selected_standard, self.compliance_percentage,)  
        self.__logger.log(20, '------------------------------------------------------------------------------------------------------------------')
        self.__logger.log(50, "| Standard | Part |                Required Work Item                          |            Location            | Fulfilled? | Work Item Location |Role Responsible")
        self.__logger.log(20, "|----------|------|------------------------------------------------------------|--------------------------------|------------|--------------------|------------------|")
        
        for result in self.compliance_results:
            self.__logger.log(20, f"| {result['Standard']} | {result['Part']}| {result['Required Work Item']} | {result['Location1']} |{result['Fulfilled?']} | {result['Location2']} | {result['Role Responsible']}")
    
    def export_compliance_report(self):                     
        # Prepare the data for the table
        data = [['Standard', 'Part', 'Required Work Item', 'Fulfilled?', 'Work Item Location','Role Responsible']]
        for result in self.compliance_results:
            data.append([result['Standard'], result['Part'], result['Required Work Item'], result['Fulfilled?'], result['Location2'],result['Role Responsible']])

        # Create a PDF document in landscape orientation
        isoname = self.selected_standard.lower().replace(" ", "_")
        filename = f"compliance/{isoname}_compliance_report.pdf"

        doc = SimpleDocTemplate(filename, pagesize=landscape(legal))

        # Adding a header for the table
        styles = getSampleStyleSheet()
        header = Paragraph("COMPLIANCE CHECK RESULTS TO %s- %s %% COMPLETE." % (self.selected_standard, self.compliance_percentage), styles['Heading1'])
        table = Table(data)

        # Styling table
        table_style = TableStyle([
            ('BACKGROUND', (0, 0), (-1, 0), colors.grey),
            ('TEXTCOLOR', (0, 0), (-1, 0), colors.black),
            ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
            ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
            ('FONTSIZE', (0, 0), (-1, 0), 14),
            ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
            ('BACKGROUND', (0, 1), (-1, -1), colors.antiquewhite),
            ('GRID', (0, 0), (-1, -1), 1, colors.black)
        ])

        

        # Style cells based on the compliance status
        for row in range(len(data)):
            if data[row][3] == "YES":
                table_style.add('TEXTCOLOR', (3, row), (3, row), colors.green)
                table_style.add('TEXTCOLOR', (4, row), (4, row), colors.green)
            elif data[row][3] == "NO":
                table_style.add('TEXTCOLOR', (3, row), (3, row), colors.red)
                table_style.add('TEXTCOLOR', (4, row), (4, row), colors.red)
        
        table.setStyle(table_style)

        
        # Build the PDF
        doc.build([header, table])

        self.__logger.log(20, '\n\n -------------------------------------------------------------------------------------------------------------')
        self.__logger.log(20, 'COMPLIANCE CHECK RESULTS EXPORTED TO %s.', filename)  
        

        return

    def prompt_options(self):
         while True:
            self.__logger.log(20, '\n\n -------------------------------------------------------------------------------------------------------------')
            user_input = input("Choose an option: \n1) Export Compliance Results \n2) Exit\nEnter the corresponding number: ")
            if user_input == '1':
                print("Exporting data...")
                self.export_compliance_report()
            elif user_input == '2':
                print("Exiting...")
                break 
            else:
                print("Invalid option. Please try again.")
         return 
    