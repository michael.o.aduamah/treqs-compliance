<treqs-element id="21e1616ada6d11ee8700c0185082f504" type="boundaryObject">
# BOUNDARY OBJECT 
<treqs-link type="coordinatesBetween" target="d2b7d5f8d63411ee81b0145afc7dd63e" />
</treqs-element>

<treqs-element id="d2b7d5f8d63411ee81b0145afc7dd63e" type="methodologicalIsland">
# METHODOLOGICAL ISLAND 
</treqs-element>

<treqs-element id="0a82ecd4d63311eeb0eb145afc7dd63e" type="governanceTeam">
# GOVERNANCE TEAM
<treqs-link type="governs" target="21e1616ada6d11ee8700c0185082f504" />
</treqs-element>

<treqs-element id="481d1fadd63311ee8edb145afc7dd63e" type="driver">
# DRIVER 
<treqs-link type="drives" target="d2b7d5f8d63411ee81b0145afc7dd63e" />
</treqs-element>

<treqs-element id="2e7d9bdcd63311eeae0c145afc7dd63e" type="role">
#  ROLE
<treqs-link type="partOf" target="d2b7d5f8d63411ee81b0145afc7dd63e" />
<treqs-link type="partOf" target="0a82ecd4d63311eeb0eb145afc7dd63e" />
<treqs-link type="responsibleFor" target="0a82ecd4d63311eeb0eb145afc7dd63e" />
<treqs-link type="creates" target="0a82ecd4d63311eeb0eb145afc7dd63e" />
<treqs-link type="updates" target="0a82ecd4d63311eeb0eb145afc7dd63e" />
<treqs-link type="deletes" target="0a82ecd4d63311eeb0eb145afc7dd63e" />


</treqs-element>



