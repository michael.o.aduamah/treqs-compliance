import unittest

from treqs.list_elements import list_elements


class TestListElements(unittest.TestCase):

    # <treqs-element id="38e94278a22f11eba9dca7925d1c5fe9" type="unittest">
    # Basic listing.
    # <treqs-link type="tests" target="a0820e06-9614-11ea-bb37-0242ac130002" />
    # <treqs-link type="tests" target="63ef8bfa76ae11ebb811cf2f044815f7" />
    # <treqs-link type="tests" target="bc89e02a76c811ebb811cf2f044815f7" />
    # </treqs-element>
    def test_list_elements(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            with self.assertRaises(SystemExit) as cm:
                le = list_elements()
                le.list_elements('tests/test_data/2-test-list-treq-elements.md', None, 'false', None, False, False)
        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(len(captured.records), 16)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_factory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(),
                         "\n\nCalling XML traversal with filename tests/test_data/2-test-list-treq-elements.md")
        self.assertEqual(captured.records[4].getMessage(),
                         "   ### Processing elements in File tests/test_data/2-test-list-treq-elements.md")
        self.assertEqual(captured.records[5].getMessage(), "| UID | Type | Label | File:Line |")
        self.assertEqual(captured.records[6].getMessage(), "| :--- | :--- | :--- | :--- |")
        self.assertEqual(captured.records[7].getMessage(),
                         "| a0820e06-9614-11ea-bb37-0242ac130002 | requirement | ### 2.0 Parameters and default output of treqs list | tests/test_data/2-test-list-treq-elements.md:2 |")
        self.assertEqual(captured.records[8].getMessage(),
                         "| 63ef8bfa76ae11ebb811cf2f044815f7 | requirement | ### 2.1 Information listed by treqs list | tests/test_data/2-test-list-treq-elements.md:11 |")
        self.assertEqual(captured.records[9].getMessage(),
                         "| 437f09c6-9613-11ea-bb37-0242ac130002 | requirement | ### 2.2 Filter by type  | tests/test_data/2-test-list-treq-elements.md:22 |")
        self.assertEqual(captured.records[10].getMessage(),
                         "| abc40962a23511eba9dca7925d1c5fe9 | information | Note that the type should usually be defined in the TIM. treqs list does however not check for this to be the case. Use treqs check instead to make sure that all types are consistent with the TIM. treqs list allows to search for invalid types. | tests/test_data/2-test-list-treq-elements.md:32 |")
        self.assertEqual(captured.records[11].getMessage(),
                         "| a0820b4a-9614-11ea-bb37-0242ac130002 | requirement | ### 2.3 Filter by ID | tests/test_data/2-test-list-treq-elements.md:39 |")
        self.assertEqual(captured.records[12].getMessage(),
                         "| bc89e02a76c811ebb811cf2f044815f7 | requirement | ### 2.4 List all elements in a file | tests/test_data/2-test-list-treq-elements.md:50 |")
        self.assertEqual(captured.records[13].getMessage(),
                         "| 638fa22e76c911ebb811cf2f044815f7 | requirement | ### 2.5 List treqs elements in a directory | tests/test_data/2-test-list-treq-elements.md:60 |")
        self.assertEqual(captured.records[14].getMessage(),
                         "| 1595ed20a27111eb8d3991dd3edc620a | requirement | ### 2.6 List outgoing tracelinks | tests/test_data/2-test-list-treq-elements.md:72 |")
        self.assertEqual(captured.records[15].getMessage(),
                         "| d9e68f9aa27b11eb8d3991dd3edc620a | requirement | ### 2.7 List incoming tracelinks | tests/test_data/2-test-list-treq-elements.md:86 |")

    # <treqs-element id="3448f8f8a23411eba9dca7925d1c5fe9" type="unittest">
    # Tests basic listing functionality when a specific type is provided.
    # <treqs-link type="tests" target="437f09c6-9613-11ea-bb37-0242ac130002" />
    # </treqs-element>
    def test_list_elements_of_type(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            with self.assertRaises(SystemExit) as cm:
                le = list_elements()
                le.list_elements('tests/test_data/2-test-list-treq-elements.md', "information",  'false', None, False, False)

        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(len(captured.records), 8)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_factory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(), "\n\nCalling XML traversal with filename tests/test_data/2-test-list-treq-elements.md")
        self.assertEqual(captured.records[4].getMessage(), "   ### Processing elements in File tests/test_data/2-test-list-treq-elements.md")
        self.assertEqual(captured.records[5].getMessage(), "| UID | Type | Label | File:Line |")
        self.assertEqual(captured.records[6].getMessage(), "| :--- | :--- | :--- | :--- |")
        self.assertEqual(captured.records[7].getMessage(), "| abc40962a23511eba9dca7925d1c5fe9 | information | Note that the type should usually be defined in the TIM. treqs list does however not check for this to be the case. Use treqs check instead to make sure that all types are consistent with the TIM. treqs list allows to search for invalid types. | tests/test_data/2-test-list-treq-elements.md:32 |")

    # <treqs-element id="8655cb6a0bfe11ec8f66f018989356c1" type="unittest">
    # Tests basic listing functionality when a specific id is provided.
    # <treqs-link type="tests" target="a0820b4a-9614-11ea-bb37-0242ac130002" />
    # </treqs-element>
    def test_list_elements_with_id(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            with self.assertRaises(SystemExit) as cm:
                le = list_elements()
                le.list_elements('tests/test_data/2-test-list-treq-elements.md', None, 'false', 'a0820b4a-9614-11ea-bb37-0242ac130002', False, False)

        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(len(captured.records), 8)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_factory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(), "\n\nCalling XML traversal with filename tests/test_data/2-test-list-treq-elements.md")
        self.assertEqual(captured.records[4].getMessage(), "   ### Processing elements in File tests/test_data/2-test-list-treq-elements.md")
        self.assertEqual(captured.records[5].getMessage(), "| UID | Type | Label | File:Line |")
        self.assertEqual(captured.records[6].getMessage(), "| :--- | :--- | :--- | :--- |")
        self.assertEqual(captured.records[7].getMessage(), "| a0820b4a-9614-11ea-bb37-0242ac130002 | requirement | ### 2.3 Filter by ID | tests/test_data/2-test-list-treq-elements.md:39 |")


    # <treqs-element id="62a684fc0bfe11ec9984f018989356c1" type="unittest">
    # List outgoing tracelinks.
    # <treqs-link type="tests" target="1595ed20a27111eb8d3991dd3edc620a" />
    # </treqs-element>
    def test_list_elements_with_outgoing_links(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            with self.assertRaises(SystemExit) as cm:
                le = list_elements()
                # Some caching problems for this test, I assume..
                le.treqs_element_factory._treqs_elements.clear()
                le.list_elements('tests/test_data/2-test-list-treq-elements.md', None, False , '1595ed20a27111eb8d3991dd3edc620a', True, False)

        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(len(captured.records), 13)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_factory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(), "\n\nCalling XML traversal with filename tests/test_data/2-test-list-treq-elements.md")
        self.assertEqual(captured.records[4].getMessage(), "   ### Processing elements in File tests/test_data/2-test-list-treq-elements.md")
        self.assertEqual(captured.records[5].getMessage(), "| UID | Type | Label | File:Line |")
        self.assertEqual(captured.records[6].getMessage(), "| :--- | :--- | :--- | :--- |")
        self.assertEqual(captured.records[7].getMessage(), "| 1595ed20a27111eb8d3991dd3edc620a | requirement | ### 2.6 List outgoing tracelinks | tests/test_data/2-test-list-treq-elements.md:72 |")
        self.assertEqual(captured.records[8].getMessage(), "| --outlink--> (35590bca-960f-11ea-bb37-0242ac130002) | hasParent | Target: Target treqs element not found. Has the containing file been included in the scope? | -- |")
        self.assertEqual(captured.records[9].getMessage(), "| --outlink--> (63ef8bfa76ae11ebb811cf2f044815f7) | relatesTo | Target: ### 2.1 Information listed by treqs list | tests/test_data/2-test-list-treq-elements.md:11 |")
        self.assertEqual(captured.records[10].getMessage(), "| --outlink--> (d9e68f9aa27b11eb8d3991dd3edc620a) | relatesTo | Target: ### 2.7 List incoming tracelinks | tests/test_data/2-test-list-treq-elements.md:86 |")
        self.assertEqual(captured.records[11].getMessage(), "| --outlink--> (1e9885f69d3311eb859fc4b301c00591) | addresses | Target: Target treqs element not found. Has the containing file been included in the scope? | -- |")
        self.assertEqual(captured.records[12].getMessage(), "| --outlink--> (54a4e59a9d3311ebb4d2c4b301c00591) | addresses | Target: Target treqs element not found. Has the containing file been included in the scope? | -- |")

    # <treqs-element id="5f38eea40bfe11ecbc65f018989356c1" type="unittest">
    # Test listing incoming tracelinks.
    # <treqs-link type="tests" target="d9e68f9aa27b11eb8d3991dd3edc620a" />
    # </treqs-element>
    def test_list_elements_with_incoming_links(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            with self.assertRaises(SystemExit) as cm:
                le = list_elements()
                le.list_elements('tests/test_data/2-test-list-treq-elements.md', None, False, 'd9e68f9aa27b11eb8d3991dd3edc620a', False, True)

        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(len(captured.records), 9)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_factory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(), "\n\nCalling XML traversal with filename tests/test_data/2-test-list-treq-elements.md")
        self.assertEqual(captured.records[4].getMessage(), "   ### Processing elements in File tests/test_data/2-test-list-treq-elements.md")
        self.assertEqual(captured.records[5].getMessage(), "| UID | Type | Label | File:Line |")
        self.assertEqual(captured.records[6].getMessage(), "| :--- | :--- | :--- | :--- |")
        self.assertEqual(captured.records[7].getMessage(), "| d9e68f9aa27b11eb8d3991dd3edc620a | requirement | ### 2.7 List incoming tracelinks | tests/test_data/2-test-list-treq-elements.md:86 |")
        self.assertEqual(captured.records[8].getMessage(), "| --inlink--> (1595ed20a27111eb8d3991dd3edc620a) | relatesTo | Source: ### 2.6 List outgoing tracelinks | tests/test_data/2-test-list-treq-elements.md:72 |")

    # Tests that treqs-elements in non-XML files are found and listed.
    def test_list_elements_in_non_xml(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            with self.assertRaises(SystemExit) as cm:
                le = list_elements()
                le.list_elements('./tests/test_data/6-test-traverse-treqs.md', None, 'false', None, False, False)

        self.assertEqual(cm.exception.code, 0)

        self.assertEqual(len(captured.records), 10)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_factory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(), "\n\nCalling XML traversal with filename ./tests/test_data/6-test-traverse-treqs.md")
        self.assertEqual(captured.records[4].getMessage(), "   ### Processing elements in File ./tests/test_data/6-test-traverse-treqs.md")
        self.assertEqual(captured.records[5].getMessage(), "| UID | Type | Label | File:Line |")
        self.assertEqual(captured.records[6].getMessage(), "| :--- | :--- | :--- | :--- |")
        self.assertEqual(captured.records[7].getMessage(), "| 0276e84ac79011ebb719f018989356c1 | requirement | ### 6-test-1 TReqs file traverser shall be able to handle documents without root tag. | ./tests/test_data/6-test-traverse-treqs.md:1 |")
        self.assertEqual(captured.records[8].getMessage(), "| ff403b04c78f11ebbdc9f018989356c1 | requirement | ### 6-test-2 TReqs file traverser shall be able to handle treqs-elements that are not under the root. | ./tests/test_data/6-test-traverse-treqs.md:9 |")
        self.assertEqual(captured.records[9].getMessage(), "| c5ae0c10c79211eb9631f018989356c1 | requirement | ### 6-test-3 TReqs file traverser shall be able to handle treqs-elements in non-md files. | ./tests/test_data/6-test-traverse-treqs.md:17 |")

    def test_list_file_pattern(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            with self.assertRaises(SystemExit) as cm:
                le = list_elements()
                le.list_elements(('./tests/test_data/6-test-traverse-treqs.md', './tests/test_data/6-test-traverse-treqs.py', './tests/test_data/test-recursive-traverser'), None, 'True', None, False, False)
        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(len(captured.records), 16)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_factory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")

        records = [r.getMessage() for r in captured.records[3:10]]
        self.assertIn("\n\nCalling XML traversal with filename ./tests/test_data/6-test-traverse-treqs.md", records)
        self.assertIn("   ### Processing elements in File ./tests/test_data/6-test-traverse-treqs.md", records)
        self.assertIn("\n\nCalling XML traversal with filename ./tests/test_data/6-test-traverse-treqs.py", records)
        self.assertIn("   ### Processing elements in File ./tests/test_data/6-test-traverse-treqs.py", records)
        self.assertIn("   ### Ignoring file tests/test_data/test-recursive-traverser/test-treq-file-2.md (.treqs-ignore)", records)
        self.assertIn("   ### Ignoring file tests/test_data/test-recursive-traverser/test-treq-file-1.md (.treqs-ignore)", records)
        self.assertIn("   ### Ignoring file tests/test_data/test-recursive-traverser/test-treq-file-3.py (.treqs-ignore)", records)
        
        self.assertEqual(captured.records[10].getMessage(), "| UID | Type | Label | File:Line |")
        self.assertEqual(captured.records[11].getMessage(), "| :--- | :--- | :--- | :--- |")
        self.assertEqual(captured.records[12].getMessage(), "| 0276e84ac79011ebb719f018989356c1 | requirement | ### 6-test-1 TReqs file traverser shall be able to handle documents without root tag. | ./tests/test_data/6-test-traverse-treqs.md:1 |")
        self.assertEqual(captured.records[13].getMessage(), "| ff403b04c78f11ebbdc9f018989356c1 | requirement | ### 6-test-2 TReqs file traverser shall be able to handle treqs-elements that are not under the root. | ./tests/test_data/6-test-traverse-treqs.md:9 |")
        self.assertEqual(captured.records[14].getMessage(), "| c5ae0c10c79211eb9631f018989356c1 | requirement | ### 6-test-3 TReqs file traverser shall be able to handle treqs-elements in non-md files. | ./tests/test_data/6-test-traverse-treqs.md:17 |")
        self.assertEqual(captured.records[15].getMessage(), "| 9c0adc12c79211ebb9cbf018989356c1 | unittest |     # Does nothing - test data. | ./tests/test_data/6-test-traverse-treqs.py:5 |")


class TestTestListElementsPlantUML(unittest.TestCase):

    # <treqs-element id="1dc5e4d6d23011eeadb88de628732e03" type="unittest">
    # Test generating PlantUML from treqs list command
    # <treqs-link type="tests" target="6473290ed22e11eeadb88de628732e03" />
    # </treqs-element>
    def test_list_elements(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            with self.assertRaises(SystemExit) as cm:
                le = list_elements()
                le.list_elements_as_plantuml('tests/test_data/2-test-list-treq-elements.md', None, True, None)
        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(len(captured.records), 83)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_factory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(),
                         "\n\nCalling XML traversal with filename tests/test_data/2-test-list-treq-elements.md")
        self.assertEqual(captured.records[4].getMessage(),
                         "   ### Processing elements in File tests/test_data/2-test-list-treq-elements.md")
        records = [r.getMessage() for r in captured.records[5:88]]
        self.assertIn('@startuml', records)
        self.assertIn(
            'map "**### 2.0 Parameters and default output of treqs list**" as a0820e06_9614_11ea_bb37_0242ac130002 {',
            records)
        self.assertIn('uid => ""a0820e06-9614-11ea-bb37-0242ac130002""', records)
        self.assertIn('type => //requirement//', records)
        self.assertIn('location => tests/\\ntest_data/\\n2-test-list-treq-elements.md:2', records)
        self.assertIn('}', records)
        self.assertIn('map "**### 2.1 Information listed by treqs list**" as 63ef8bfa76ae11ebb811cf2f044815f7 {',
                      records)
        self.assertIn('uid => ""63ef8bfa76ae11ebb811cf2f044815f7""', records)
        self.assertIn('type => //requirement//', records)
        self.assertIn('location => tests/\\ntest_data/\\n2-test-list-treq-elements.md:11', records)
        self.assertIn('}', records)
        self.assertIn('map "**### 2.2 Filter by type **" as 437f09c6_9613_11ea_bb37_0242ac130002 {', records)
        self.assertIn('uid => ""437f09c6-9613-11ea-bb37-0242ac130002""', records)
        self.assertIn('type => //requirement//', records)
        self.assertIn('location => tests/\\ntest_data/\\n2-test-list-treq-elements.md:22', records)
        self.assertIn('}', records)
        self.assertIn(
            'map "**Note that the type should usually be defined in the TIM. treqs list does however not check for this to be the case. Use treqs check instead to make sure that all types are consistent with the TIM. treqs list allows to search for invalid types.**" as abc40962a23511eba9dca7925d1c5fe9 {',
            records)
        self.assertIn('uid => ""abc40962a23511eba9dca7925d1c5fe9""', records)
        self.assertIn('type => //information//', records)
        self.assertIn('location => tests/\\ntest_data/\\n2-test-list-treq-elements.md:32', records)
        self.assertIn('}', records)
        self.assertIn('map "**### 2.3 Filter by ID**" as a0820b4a_9614_11ea_bb37_0242ac130002 {', records)
        self.assertIn('uid => ""a0820b4a-9614-11ea-bb37-0242ac130002""', records)
        self.assertIn('type => //requirement//', records)
        self.assertIn('location => tests/\\ntest_data/\\n2-test-list-treq-elements.md:39', records)
        self.assertIn('}', records)
        self.assertIn('map "**### 2.4 List all elements in a file**" as bc89e02a76c811ebb811cf2f044815f7 {', records)
        self.assertIn('uid => ""bc89e02a76c811ebb811cf2f044815f7""', records)
        self.assertIn('type => //requirement//', records)
        self.assertIn('location => tests/\\ntest_data/\\n2-test-list-treq-elements.md:50', records)
        self.assertIn('}', records)
        self.assertIn('map "**### 2.5 List treqs elements in a directory**" as 638fa22e76c911ebb811cf2f044815f7 {',
                      records)
        self.assertIn('uid => ""638fa22e76c911ebb811cf2f044815f7""', records)
        self.assertIn('type => //requirement//', records)
        self.assertIn('location => tests/\\ntest_data/\\n2-test-list-treq-elements.md:60', records)
        self.assertIn('}', records)
        self.assertIn('map "**### 2.6 List outgoing tracelinks**" as 1595ed20a27111eb8d3991dd3edc620a {', records)
        self.assertIn('uid => ""1595ed20a27111eb8d3991dd3edc620a""', records)
        self.assertIn('type => //requirement//', records)
        self.assertIn('location => tests/\\ntest_data/\\n2-test-list-treq-elements.md:72', records)
        self.assertIn('}', records)
        self.assertIn('map "**### 2.7 List incoming tracelinks**" as d9e68f9aa27b11eb8d3991dd3edc620a {', records)
        self.assertIn('uid => ""d9e68f9aa27b11eb8d3991dd3edc620a""', records)
        self.assertIn('type => //requirement//', records)
        self.assertIn('location => tests/\\ntest_data/\\n2-test-list-treq-elements.md:86', records)
        self.assertIn('}', records)
        self.assertIn('bc89e02a76c811ebb811cf2f044815f7 --> a0820e06_9614_11ea_bb37_0242ac130002 : relatesTo', records)
        self.assertIn('1595ed20a27111eb8d3991dd3edc620a --> 63ef8bfa76ae11ebb811cf2f044815f7 : relatesTo', records)
        self.assertIn('d9e68f9aa27b11eb8d3991dd3edc620a --> 63ef8bfa76ae11ebb811cf2f044815f7 : relatesTo', records)
        self.assertIn('638fa22e76c911ebb811cf2f044815f7 --> bc89e02a76c811ebb811cf2f044815f7 : relatesTo', records)
        self.assertIn('1595ed20a27111eb8d3991dd3edc620a --> d9e68f9aa27b11eb8d3991dd3edc620a : relatesTo', records)
        self.assertIn('map "**OUT OF SCOPE ELEMENT**" as 35590bca_960f_11ea_bb37_0242ac130002 {', records)
        self.assertIn('uid => ""35590bca-960f-11ea-bb37-0242ac130002""', records)
        self.assertIn('}', records)
        self.assertIn('a0820e06_9614_11ea_bb37_0242ac130002 --> 35590bca_960f_11ea_bb37_0242ac130002 : hasParent',
                      records)
        self.assertIn('map "**OUT OF SCOPE ELEMENT**" as 1e9885f69d3311eb859fc4b301c00591 {', records)
        self.assertIn('uid => ""1e9885f69d3311eb859fc4b301c00591""', records)
        self.assertIn('}', records)
        self.assertIn('a0820e06_9614_11ea_bb37_0242ac130002 --> 1e9885f69d3311eb859fc4b301c00591 : addresses', records)
        self.assertIn('63ef8bfa76ae11ebb811cf2f044815f7 --> 35590bca_960f_11ea_bb37_0242ac130002 : hasParent', records)
        self.assertIn('63ef8bfa76ae11ebb811cf2f044815f7 --> 1e9885f69d3311eb859fc4b301c00591 : addresses', records)
        self.assertIn('437f09c6_9613_11ea_bb37_0242ac130002 --> 35590bca_960f_11ea_bb37_0242ac130002 : hasParent',
                      records)
        self.assertIn('437f09c6_9613_11ea_bb37_0242ac130002 --> 1e9885f69d3311eb859fc4b301c00591 : addresses', records)
        self.assertIn('a0820b4a_9614_11ea_bb37_0242ac130002 --> 35590bca_960f_11ea_bb37_0242ac130002 : hasParent',
                      records)
        self.assertIn('a0820b4a_9614_11ea_bb37_0242ac130002 --> 1e9885f69d3311eb859fc4b301c00591 : addresses', records)
        self.assertIn('map "**OUT OF SCOPE ELEMENT**" as 54a4e59a9d3311ebb4d2c4b301c00591 {', records)
        self.assertIn('uid => ""54a4e59a9d3311ebb4d2c4b301c00591""', records)
        self.assertIn('}', records)
        self.assertIn('a0820b4a_9614_11ea_bb37_0242ac130002 --> 54a4e59a9d3311ebb4d2c4b301c00591 : addresses', records)
        self.assertIn('bc89e02a76c811ebb811cf2f044815f7 --> 35590bca_960f_11ea_bb37_0242ac130002 : hasParent', records)
        self.assertIn('bc89e02a76c811ebb811cf2f044815f7 --> a0820e06_9614_11ea_bb37_0242ac130002 : relatesTo', records)
        self.assertIn('638fa22e76c911ebb811cf2f044815f7 --> 35590bca_960f_11ea_bb37_0242ac130002 : hasParent', records)
        self.assertIn('638fa22e76c911ebb811cf2f044815f7 --> bc89e02a76c811ebb811cf2f044815f7 : relatesTo', records)
        self.assertIn('1595ed20a27111eb8d3991dd3edc620a --> 35590bca_960f_11ea_bb37_0242ac130002 : hasParent', records)
        self.assertIn('1595ed20a27111eb8d3991dd3edc620a --> 63ef8bfa76ae11ebb811cf2f044815f7 : relatesTo', records)
        self.assertIn('1595ed20a27111eb8d3991dd3edc620a --> d9e68f9aa27b11eb8d3991dd3edc620a : relatesTo', records)
        self.assertIn('1595ed20a27111eb8d3991dd3edc620a --> 1e9885f69d3311eb859fc4b301c00591 : addresses', records)
        self.assertIn('1595ed20a27111eb8d3991dd3edc620a --> 54a4e59a9d3311ebb4d2c4b301c00591 : addresses', records)
        self.assertIn('d9e68f9aa27b11eb8d3991dd3edc620a --> 35590bca_960f_11ea_bb37_0242ac130002 : hasParent', records)
        self.assertIn('d9e68f9aa27b11eb8d3991dd3edc620a --> 63ef8bfa76ae11ebb811cf2f044815f7 : relatesTo', records)
        self.assertIn('d9e68f9aa27b11eb8d3991dd3edc620a --> 1e9885f69d3311eb859fc4b301c00591 : addresses', records)
        self.assertIn('d9e68f9aa27b11eb8d3991dd3edc620a --> 54a4e59a9d3311ebb4d2c4b301c00591 : addresses', records)
        self.assertIn('@enduml', records)


if __name__ == "__main__":
    unittest.main()
