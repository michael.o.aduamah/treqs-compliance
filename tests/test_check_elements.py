import unittest

from treqs.check_elements import check_elements

class TestCheckElements(unittest.TestCase):

    # <treqs-element id="e9db9d24920d11eba831f018989356c1" type="unittest">
    # setup and successful checks of ./test_data/5-test-faulty-types-and-links.md
    # <treqs-link type="tests" target="3cff0d2a919511eb8becf018989356c1" />
    # <treqs-link type="tests" target="951ecc70919511eb978ff018989356c1" />
    # <treqs-link type="tests" target="a787f400223011ecb43ef018989356c1" />
    # <treqs-link type="tests" target="a787f3a6223011ecb43ef018989356c1" />
    # <treqs-link type="tests" target="a787f342223011ecb43ef018989356c1" />
    # <treqs-link type="tests" target="a787f20c223011ecb43ef018989356c1" />
    # </treqs-element>
    def test_check_elements(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            with self.assertRaises(SystemExit) as cm:
                ce = check_elements()
                ce.check_elements('./tests/test_data/5-test-faulty-types-and-links.md', False, './ttim.yaml')

        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(len(captured.records), 20)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_factory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(), "check_elements created")
        self.assertEqual(captured.records[4].getMessage(), "Processing TTIM at ./ttim.yaml")
        self.assertEqual(captured.records[5].getMessage(), "Types and their links according to TTIM: {'requirement': {'relatesTo': None, 'hasParent': 'requirement', 'addresses': 'stakeholder-requirement'}, 'stakeholder-need': {}, 'stakeholder-requirement': {'addresses': 'stakeholder-need', 'relatesTo': 'stakeholder-requirement'}, 'unittest': {'relatesTo': None, 'hasParent': 'unittest', 'tests': 'requirement'}, 'information': {'relatesTo': None}}")
        self.assertEqual(captured.records[6].getMessage(), "\n\nCalling XML traversal with filename ./tests/test_data/5-test-faulty-types-and-links.md")
        self.assertEqual(captured.records[7].getMessage(), "   ### Processing elements in File ./tests/test_data/5-test-faulty-types-and-links.md")
        self.assertEqual(captured.records[8].getMessage(), "| Error location | Error | File:Line |")
        self.assertEqual(captured.records[9].getMessage(), "| :--- | :--- | :--- |")
        self.assertEqual(captured.records[10].getMessage(), "| Element e770de36920911eb9355f018989356c1 | Element has an unrecognized type: non-existing-type | ./tests/test_data/5-test-faulty-types-and-links.md:2 |")
        self.assertEqual(captured.records[11].getMessage(), "| Element 2c600896920a11ebbb6ff018989356c1 | Unrecognised link type relatesToo within element of type requirement. | ./tests/test_data/5-test-faulty-types-and-links.md:9 |")
        self.assertEqual(captured.records[12].getMessage(), "| Element 56cbd2e0920a11ebb9d1f018989356c1 | Unrecognised link type tests within element of type requirement. | ./tests/test_data/5-test-faulty-types-and-links.md:16 |")
        self.assertEqual(captured.records[13].getMessage(), "| Element 9d3a98c80bd711ec8e3ff018989356c1 | Element has an unrecognized type:  | ./tests/test_data/5-test-faulty-types-and-links.md:58 |")
        self.assertEqual(captured.records[14].getMessage(), "| Element 4f5bcadad45711eb9de4f018989356c1 | Element references non-existent element with id inexistent_target_id | ./tests/test_data/5-test-faulty-types-and-links.md:45 |")
        self.assertEqual(captured.records[15].getMessage(), "| Element 3cabd686d45811ebaaeef018989356c1 | 'addresses' link to element 4f5bcadad45711eb9de4f018989356c1 needs to point to a stakeholder-requirement, but points to a requirement instead. | ./tests/test_data/5-test-faulty-types-and-links.md:51 |")
        self.assertEqual(captured.records[16].getMessage(), "| Element 940f4d62920a11eba034f018989356c1 | Element id is duplicated. | ./tests/test_data/5-test-faulty-types-and-links.md:35 |")
        self.assertEqual(captured.records[17].getMessage(), "| Element type requirement | Element does not have an id. | ./tests/test_data/5-test-faulty-types-and-links.md:23 |")
        self.assertEqual(captured.records[18].getMessage(), "| Element 9d3a98c80bd711ec8e3ff018989356c1 | Element has an empty or missing type. | ./tests/test_data/5-test-faulty-types-and-links.md:58 |")
        self.assertEqual(captured.records[19].getMessage(), "treqs check exited with failed checks.")

    # <treqs-element id="a813ac0c0bd811ecae3df018989356c1" type="unittest">
    # setup and successful checks of ./test_data/5-test-successful-check.md
    # <treqs-link type="tests" target="c5402c1a919411eb8311f018989356c1" />
    # </treqs-element>
    def test_successfully_check_elements(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            with self.assertRaises(SystemExit) as cm:
                ce = check_elements()
                ce.check_elements('./tests/test_data/5-test-successful-check.md', False, './ttim.yaml')

        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(len(captured.records), 11)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_factory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(), "check_elements created")
        self.assertEqual(captured.records[4].getMessage(), "Processing TTIM at ./ttim.yaml")
        self.assertEqual(captured.records[5].getMessage(), "Types and their links according to TTIM: {'requirement': {'relatesTo': None, 'hasParent': 'requirement', 'addresses': 'stakeholder-requirement'}, 'stakeholder-need': {}, 'stakeholder-requirement': {'addresses': 'stakeholder-need', 'relatesTo': 'stakeholder-requirement'}, 'unittest': {'relatesTo': None, 'hasParent': 'unittest', 'tests': 'requirement'}, 'information': {'relatesTo': None}}")
        self.assertEqual(captured.records[6].getMessage(), "\n\nCalling XML traversal with filename ./tests/test_data/5-test-successful-check.md")
        self.assertEqual(captured.records[7].getMessage(), "   ### Processing elements in File ./tests/test_data/5-test-successful-check.md")
        self.assertEqual(captured.records[8].getMessage(), "| Error location | Error | File:Line |")
        self.assertEqual(captured.records[9].getMessage(), "| :--- | :--- | :--- |")
        self.assertEqual(captured.records[10].getMessage(), "treqs check exited successfully.")

    # <treqs-element id="fec54ee40c0411eca67ff018989356c1" type="unittest">
    # tests that treqs check fails when ttim is not found
    # <treqs-link type="tests" target="c5402c1a919411eb8311f018989356c1" />
    # </treqs-element>
    def test_fail_for_missing_ttim(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            with self.assertRaises(SystemExit) as cm:
                ce = check_elements()
                ce.check_elements('./tests/test_data/5-test-successful-check.md', False, './ttim_missing.yaml')

        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(len(captured.records), 7)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_factory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(), "check_elements created")
        self.assertEqual(captured.records[4].getMessage(), "Processing TTIM at ./ttim_missing.yaml")
        self.assertEqual(captured.records[5].getMessage(), "TTIM could not be loaded at ./ttim_missing.yaml")
        self.assertEqual(captured.records[6].getMessage(), "treqs check exited with failed checks.")

    # <treqs-element id="31ae24ac223011ec8859f018989356c1" type="unittest">
    # tests that treqs check fails when ttim is not found
    # <treqs-link type="tests" target="c5402c1a919411eb8311f018989356c1" />
    # <treqs-link type="tests" target="a787f400223011ecb43ef018989356c1" />
    # </treqs-element>
    def test_check_links(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            with self.assertRaises(SystemExit) as cm:
                ce = check_elements()
                ce.check_elements('./tests/test_data/7-test-faulty-inlinks-outlinks.md', False, './ttim.yaml')

        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(len(captured.records),17)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_factory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(), "check_elements created")
        self.assertEqual(captured.records[4].getMessage(), "Processing TTIM at ./ttim.yaml")
        self.assertEqual(captured.records[5].getMessage(), "Types and their links according to TTIM: {'requirement': {'relatesTo': None, 'hasParent': 'requirement', 'addresses': 'stakeholder-requirement'}, 'stakeholder-need': {}, 'stakeholder-requirement': {'addresses': 'stakeholder-need', 'relatesTo': 'stakeholder-requirement'}, 'unittest': {'relatesTo': None, 'hasParent': 'unittest', 'tests': 'requirement'}, 'information': {'relatesTo': None}}")
        self.assertEqual(captured.records[6].getMessage(), "\n\nCalling XML traversal with filename ./tests/test_data/7-test-faulty-inlinks-outlinks.md")
        self.assertEqual(captured.records[7].getMessage(), "   ### Processing elements in File ./tests/test_data/7-test-faulty-inlinks-outlinks.md")
        self.assertEqual(captured.records[8].getMessage(), "| Error location | Error | File:Line |")
        self.assertEqual(captured.records[9].getMessage(), "| :--- | :--- | :--- |")
        self.assertEqual(captured.records[10].getMessage(), "| Element abeb5e8ce0b711ebaa057085c2221ca0 | Unrecognised link type NoType within element of type requirement. | ./tests/test_data/7-test-faulty-inlinks-outlinks.md:3 |")
        self.assertEqual(captured.records[11].getMessage(), "| Element abeb5e8ce0b711ebaa057085c2221ca0 | Unrecognised link type WrongType within element of type requirement. | ./tests/test_data/7-test-faulty-inlinks-outlinks.md:3 |")
        self.assertEqual(captured.records[12].getMessage(), "| Element afca2be7e0b711eb9b8d7085c2221ca0 |  Required links missing: ['tests'] | ./tests/test_data/7-test-faulty-inlinks-outlinks.md:28 |")
        self.assertEqual(captured.records[13].getMessage(), "| Element abeb5e8ce0b711ebaa057085c2221ca0 | 'hasParent' link to element 48773577e0c611eb85447085c2221ca0 needs to point to a requirement, but points to a stakeholder-requirement instead. | ./tests/test_data/7-test-faulty-inlinks-outlinks.md:3 |")
        self.assertEqual(captured.records[14].getMessage(), "| Element ae05cc8de0b711eb83b37085c2221ca0 | 'addresses' link to element afca2be7e0b711eb9b8d7085c2221ca0 needs to point to a stakeholder-need, but points to a unittest instead. | ./tests/test_data/7-test-faulty-inlinks-outlinks.md:18 |")
        self.assertEqual(captured.records[15].getMessage(), "| Element ae05cc8de0b711eb83b37085c2221ca0 | 'addresses' link to element abeb5e8ce0b711ebaa057085c2221ca0 needs to point to a stakeholder-need, but points to a requirement instead. | ./tests/test_data/7-test-faulty-inlinks-outlinks.md:18 |")
        self.assertEqual(captured.records[16].getMessage(), "treqs check exited with failed checks.")

    def test_check_duplicate_ids_across_multiple_files(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            with self.assertRaises(SystemExit) as cm:
                ce = check_elements()
                ce.check_elements('./tests/test_data/duplicate-ids', False, './ttim.yaml')

        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(len(captured.records), 15)
        records = [r.getMessage() for r in captured.records]
        self.assertIn(
            '| Element a-very-unique-id | Element id is duplicated. | tests/test_data/duplicate-ids/file_one.md:6 |',
            records)
        self.assertIn(
            '| Element a-very-unique-id | Element id is duplicated. | tests/test_data/duplicate-ids/file_two.md:1 |',
            records)
        self.assertIn('treqs check exited with failed checks.', records)
    # <treqs-element id="3bd1feb262a311ee98f915f1b0e25002" type="unittest">
    # tests that treqs check accepts and processes links with multiple target types
    # <treqs-link type="tests" target="d00b0858628a11ee98f915f1b0e25002" />
    # </treqs-element>
    def test_multiple_targets(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            with self.assertRaises(SystemExit) as cm:
                ce = check_elements()
                ce.check_elements('./tests/test_data/multiple-targets.md', False, './tests/test_data/multiple-targets-ttim.yaml')

            self.assertEqual(cm.exception.code, 0)
            self.assertEqual(len(captured.records), 13)
            records = [r.getMessage() for r in captured.records]
            self.assertIn("Processing TTIM at ./tests/test_data/multiple-targets-ttim.yaml", records)
            self.assertIn("Types and their links according to TTIM: {'A': {'relatesTo': ['B', 'C']}, 'B': {}, 'C': {}}", records)
            self.assertIn("\n\nCalling XML traversal with filename ./tests/test_data/multiple-targets.md", records)
            self.assertIn("   ### Processing elements in File ./tests/test_data/multiple-targets.md", records)
            self.assertIn("| Error location | Error | File:Line |", records)
            self.assertIn("| :--- | :--- | :--- |", records)
            self.assertIn("treqs check exited successfully.", records)


if __name__ == "__main__":
    unittest.main()
