<treqs-element id="a" type="A">
@startuml
usecase "a: A" as a
usecase "b: B" as b
usecase "c: C" as c

a --> b : relatesTo
a -> c : relatesTo
@enduml

<treqs-link type="relatesTo" target="b" />
<treqs-link type="relatesTo" target="c" />
</treqs-element>

<treqs-element id="b" type="B">
</treqs-element>

<treqs-element id="c" type="C">
</treqs-element>
